//
//  PickupLocationModel.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 12/7/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation


class PickupLocationModel {
    
    var latitude: String
    var longitude: String
    var altitude: String
    var locationName: String
    var pickupZone: String
    var subZone: String
    var upZila: String
    var palceName : String
    var division: String
    var isIncity: Bool
    
    init() {
        latitude = ""
        longitude = ""
        altitude = ""
        locationName = ""
        pickupZone = ""
        subZone = ""
        division = ""
        upZila = ""
        palceName = ""
        isIncity = false
    }
    
    init(latitude: String, longitude: String, altitude: String, locationName: String, pickupZone: String,Subzone: String, Division: String,IsIncity: Bool,PlaceName: String, Upzila: String) {
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.locationName = locationName
        self.pickupZone = pickupZone
        self.subZone = Subzone
        self.division = Division
        self.isIncity = IsIncity
        self.upZila = Upzila
        self.palceName = PlaceName
    }
}



