//
//  vahicleModelList.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 11/12/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation
struct Vehicle {
    let success: Bool?
    let code: Int?
    let data: [Datum]?
}

struct Datum {
    let id: Int?
    let name: String?
    let allowSchedule, requestType: Int?
    let vehicleTypes: [VehicleType]?
}

struct VehicleType {
    let vehicleTypeID: Int?
    let vehicleType: String?
    let imageURL, selectImageURL: String?
    let capacity, description: String?
    let noPromoPrice, totalPrice, promoPrice: Int?
    let isPromoApplied: Bool?
    let promoCode, allowSchedule, returnPrice, oneDayExtra: Int?
    let time: String?
}
