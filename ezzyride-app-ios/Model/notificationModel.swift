//
//  notificationModel.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 22/1/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation
struct FullInfo: Codable {
    let gcmMessageID, subtitle, smallIcon, largeIcon: String
    let title, vibrate, message, sound: String
    let tickerText: String
    let aps: Aps
    
    enum CodingKeys: String, CodingKey {
        case gcmMessageID = "gcm.message_id"
        case subtitle, smallIcon, largeIcon, title, vibrate, message, sound, tickerText, aps
    }
}

struct Aps: Codable {
    let contentAvailable: String
    
    enum CodingKeys: String, CodingKey {
        case contentAvailable = "content-available"
    }
}
struct Message: Codable {
    let action: String
    let msg: Msg
}

struct Msg: Codable {
    let headline, subhead, brief: String
    let contentID: Int
    
    enum CodingKeys: String, CodingKey {
        case headline, subhead, brief
        case contentID = "content_id"
    }
}
