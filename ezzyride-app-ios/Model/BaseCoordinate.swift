//
//  BaseCoordinate.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 10/27/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation

class BaseCoordinate {
    
    var type: String
    var features: [Feature]
    var source: String
    
    init() {
        type = ""
        features = [Feature]()
        source = ""
    }
    
    init(type: String, features: [Feature], source: String) {
        self.type = type
        self.features = features
        self.source = source
    }
}



