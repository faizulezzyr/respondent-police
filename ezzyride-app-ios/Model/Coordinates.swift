//
//  Coordinates.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 10/27/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation

class Coordinates {
    
    var coordinateList: [Coordinate]
    
    init() {
        coordinateList = [Coordinate]()
    }
    
    init(coordinateList: [Coordinate]) {
        self.coordinateList = coordinateList
    }
}


