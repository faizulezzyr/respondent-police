//
//  AboutViewController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 11/6/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

class AboutViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var history: [JSON] = []
    var apiCommunicatorHelper: APICommunicator?
    var userService: UserService?
    override func viewDidLoad() {
        super.viewDidLoad()
        initApiCommunicatorHelper()
         getHistory()
        
    }
    func initApiCommunicatorHelper() {
        self.apiCommunicatorHelper = APICommunicator(view: self.view)
        self.apiCommunicatorHelper?.delegate = self
        self.userService = UserService(self.view, communicator: apiCommunicatorHelper!)
       
    }
    
    func getHistory(){
        let uId = Helpers.getIntValueForKey(Constants.USER_ID)
        userService?.getUserTripHistory(member_id: uId)
    }
    func getHistoryResponse(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            print(response)
           if !response.isEmpty {
            if let data = response["data"].array{
                self.history = data
                tableView.reloadData()
             }
           }else{
            
            }
        }
    }
    
    @IBAction func back(_ sender: Any) {
      navaigationToHome()
    }
    func navaigationToHome() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let homeController = mainStoryboard.instantiateViewController(withIdentifier: "HomeV") as? HomeController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [homeController!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }

    
    
}
extension  AboutViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return history.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! historyCell
        if let rDate = history[indexPath.row]["requested_date"].string{
            cell.date.text = "Date: \(rDate)"
        }
        if let rTime = history[indexPath.row]["requested_time"].string{
            cell.time.text = "Time: \(rTime)"
        }
        if let vName = history[indexPath.row]["passenger"].string{
            cell.victimName.text = "Victim Name: \(vName)"
        }
        if let vLocation = history[indexPath.row]["location_name"].string{
            cell.location.text = "Location: \(vLocation)"
        }
        
        return cell
    }
    
    
}
extension AboutViewController: APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
           getHistoryResponse(data, statusCode: statusCode)
        }
    }
}
