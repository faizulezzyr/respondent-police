//
//  historyCell.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 3/10/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit

class historyCell: UITableViewCell {
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var victimName: UILabel!
    @IBOutlet weak var location: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
