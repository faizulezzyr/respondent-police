//
//  GetStartedController.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 11/5/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit


class GetStartedController: UIViewController {

    
    @IBAction func getStartedButtonPressed(_ sender: UIButton) {
       animation()
    }
    
    //Animation function for taped on Get strart
    func animation(){
        UIView.animate(withDuration: 0.2, animations: {() -> Void in
 
            self.BottomView.frame.origin.y = 700
         
        }, completion: {(_ finished: Bool) -> Void in
           self.initializePhoneViewController()
        })
    }
    
    // initialize Phone View controller
    func initializePhoneViewController() {
        let introSlider: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let phoneViewController = introSlider.instantiateViewController(withIdentifier: "PhoneNumberVc") as? PhoneNumberController
        self.navigationController?.pushViewController(phoneViewController!, animated: true)
    }
    
    

    @IBOutlet weak var BottomView: UIView!
    

    
    @IBOutlet weak var getStartedButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        intiSetup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        UIView.animate(withDuration: 0.5, delay: 0.4, options: [], animations: {
            self.BottomView.center.y += -self.view.bounds.height
//            self.ImageHolder.center.y = self.view.bounds.height
        }, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func intiSetup() {
        
        getStartedButton.layer.cornerRadius = getStartedButton.frame.size.height / 2.0
        getStartedButton.layer.masksToBounds = true
   
    }
}









