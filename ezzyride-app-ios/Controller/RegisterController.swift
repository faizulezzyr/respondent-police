//
//  RegisterController.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 11/10/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegisterController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource{

    
     var data =  ["A+","A-","B+","B-","O+","O-","AB+","AB-","I don't know"]
        let customPiker = UIPickerView()
    
    @IBAction func back(_ sender: Any) {
         _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func maleButtonPressed(_ sender: UIButton) {
        
        maleRadioButton.setImage(checkedImage, for: .normal)
        femaleRadioButton.setImage(uncheckedImage, for: .normal)
        othersRadioButton.setImage(uncheckedImage, for: .normal)
        selectedRadioButton = 1
    }
    
    @IBAction func femaleButtonPressed(_ sender: UIButton) {
        
        maleRadioButton.setImage(uncheckedImage, for: .normal)
        femaleRadioButton.setImage(checkedImage, for: .normal)
        othersRadioButton.setImage(uncheckedImage, for: .normal)
        selectedRadioButton = 0
    }
    
    @IBAction func othersButtonPressed(_ sender: UIButton) {
        
        maleRadioButton.setImage(uncheckedImage, for: .normal)
        femaleRadioButton.setImage(uncheckedImage, for: .normal)
        othersRadioButton.setImage(checkedImage, for: .normal)
        selectedRadioButton = 2
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        
        let firstName = firstNameTextField.text ?? ""
        let lastName = lastNameTextField.text ?? ""
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        let confirmPassword = confirmPasswordTextField.text ?? ""
        let eContactNum = eContactNumber.text ?? ""
        let eContactN = eContactName.text ?? ""
        let eContactR = eContactRelation.text ?? ""
        
        if firstName.count > 0 && lastName.count > 0 && password.count > 0 && confirmPassword.count > 0 && selectedRadioButton != -1 && eContactNum.count > 11 && eContactN.count > 1 {
            if password == confirmPassword {
                doRegisterUser(firstName, lastName: lastName, gender: selectedRadioButton, email: email, password: password)
            }
            else {
                let popup = CustomAlertPopup.customOkayPopup("", msgBody: Constants.PASSWORD_DID_NOT_MATCH)
                self.present(popup, animated: true, completion: nil)
            }
        }
    }
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var maleRadioButton: UIButton!
    
    @IBOutlet weak var femaleRadioButton: UIButton!
    
    @IBOutlet weak var othersRadioButton: UIButton!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var bloodGroup: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var eContactName: UITextField!
    @IBOutlet weak var eContactRelation: UITextField!
    
    
    @IBOutlet weak var eContactNumber: UITextField!
    
    
    
    let checkedImage = UIImage(named: "ic_radio_button_checked")
    let uncheckedImage = UIImage(named: "ic_radio_button_unchecked")
    
    var selectedRadioButton = -1

    var mobileNumber = ""
    
    var authenticationService: AuthenticationService?
    var apiCommunicatorHelper: APICommunicator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
        initApiCommunicatorHelper()
        custopikerData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        firstNameTextField.setBottomBorder(Colors.WHITE_COLOR)
        lastNameTextField.setBottomBorder(Colors.WHITE_COLOR)
        emailTextField.setBottomBorder(Colors.WHITE_COLOR)
        passwordTextField.setBottomBorder(Colors.WHITE_COLOR)
        confirmPasswordTextField.setBottomBorder(Colors.WHITE_COLOR)
        bloodGroup.setBottomBorder(Colors.WHITE_COLOR)
        address.setBottomBorder(Colors.WHITE_COLOR)
        eContactName.setBottomBorder(Colors.WHITE_COLOR)
        eContactRelation.setBottomBorder(Colors.WHITE_COLOR)
        eContactNumber.setBottomBorder(Colors.WHITE_COLOR)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return data.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        bloodGroup.text = data[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
    func custopikerData(){
        customPiker.delegate = self
        bloodGroup.inputView = customPiker
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        authenticationService = AuthenticationService(self.view, communicator: apiCommunicatorHelper!)
    }
    
    func initSetup() {
        
        maleRadioButton.setImage(uncheckedImage, for: .normal)
        femaleRadioButton.setImage(uncheckedImage, for: .normal)
        othersRadioButton.setImage(uncheckedImage, for: .normal)
        
        nextButton.layer.cornerRadius = 20
        nextButton.layer.masksToBounds = true
    }
    
    //MARK: API Implementation
    
    func doRegisterUser(_ firstName: String, lastName: String, gender: Int, email: String, password: String) {
        let params = [
            "first_name": firstName,
            "last_name": lastName,
            "device_id": Commons.getDeviceId(),
            "email": email,
            "mobile_no": mobileNumber,
            "password": password,
            "user_type": String(UserType.One.rawValue),
            "gender": String(gender),
            "authentication_code": Constants.USER_AUTH_CODE
        ]
    
        authenticationService?.doRegisterUser(params)
    }
    
    func getRegisterResponse(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            if !response.isEmpty {
                if let token = response["token"].string {
                    Helpers.setStringValueWithKey(token, key: Constants.ACCESS_TOKEN)
                }
                
                if let member_type = response["member_type"].int {
                    Helpers.setIntValueWithKey(member_type, key: Constants.MEMBER_TYPE)
                }
                else if let member_type = response["member_type"].string {
                    Helpers.setIntValueWithKey(Int(member_type)!, key: Constants.MEMBER_TYPE)
                }
                
                if let member_id = response["member_id"].int {
                    Helpers.setIntValueWithKey(member_id, key: Constants.MEMBER_ID)
                }
                else if let member_id = response["member_id"].string {
                    Helpers.setIntValueWithKey(Int(member_id)!, key: Constants.MEMBER_ID)
                }
                
                if let user_id = response["user_id"].int {
                    Helpers.setIntValueWithKey(user_id, key: Constants.USER_ID)
                }
                else if let user_id = response["user_id"].string {
                    Helpers.setIntValueWithKey(Int(user_id)!, key: Constants.USER_ID)
                }
                navigateToDashboard()
            }
        }
        else {
            //need to handle
        }
    }
    
    func navigateToDashboard() {
        let main: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home = main.instantiateViewController(withIdentifier: "HomeV") as? HomeController
        let navigationController = UINavigationController(rootViewController: home!)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
}

extension RegisterController: APICommunicatorDelegate {
    
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
            getRegisterResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.SECOND {
            //getUserZoneResponse(data, statusCode: statusCode)
        }
    }
}













