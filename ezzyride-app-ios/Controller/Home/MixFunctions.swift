//
//  File.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 4/2/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import SwiftyJSON
import CoreLocation
import GoogleMaps
import GoogleUtilities
extension HomeController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
//    func textViewDidEndEditing(_ textView: UITextView) {
//        if textView.text.isEmpty {
//            textView.text = "Enter your Details here..."
//            textView.textColor = UIColor.lightGray
//        }
//    }
    
    func showCancelAlart() {
        
        let alertController = UIAlertController(title: "", message: "Trip cancel", preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: "Cancel", style: .destructive, handler: {
            alert -> Void in
            
            
        })
        if cancelRequestLabel == 1 {
            let cancelAction = UIAlertAction(title: "Keep searching", style: .cancel, handler: {
                (action : UIAlertAction!) -> Void in
            })
            
            alertController.addAction(cancelAction)
        }else if cancelRequestLabel == 2 {
            let cancelAction = UIAlertAction(title: "Keep Riding", style: .cancel, handler: {
                (action : UIAlertAction!) -> Void in
            })
            alertController.addAction(cancelAction)
        }
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)
    }

  
    
}
