////
////  DriverDetailsViewController.swift
////  ezzyride-app-ios
////
////  Created by Innovadeaus on 14/1/19.
////  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
////
//
//import UIKit
//import PullUpController
//import SwiftyJSON
//var msgAction = ""
//var picuplocationForDriverDetails = ""
//var destinationlocationForDriverDetailse = ""
//var totalFareForDriverDetails = 0
//var CurrentFare = 0
//var driverChanelName = ""
//class DriverDetailsViewController: PullUpController {
//    @IBOutlet private weak var visualEffectView: UIVisualEffectView!
//    @IBOutlet private weak var searchBoxContainerView: UIView!
//    @IBOutlet private weak var firstPreviewView: UIView!
//    @IBOutlet private weak var secondPreviewView: UIView!
//    //// Slide view for Driver info
//    
//    @IBOutlet weak var cancelRequest: UIButton!
//    @IBOutlet weak var passengerTotalFare: UILabel!
//    @IBOutlet weak var driverCarType: UILabel!
//    @IBOutlet weak var driverCarImage: UIImageView!
//    @IBOutlet weak var rideStageMessage: UILabel!
//    @IBOutlet weak var driverImage: UIImageView!
//    @IBOutlet weak var driverName: UILabel!
//    @IBOutlet weak var driverRating: UILabel!
//    @IBOutlet weak var callButton: UIButton!
//    @IBOutlet weak var totalTrip: UILabel!
//    @IBOutlet weak var carName: UILabel!
//    @IBOutlet weak var carNumber: UILabel!
//    @IBOutlet weak var nameView: UIView!
//    @IBOutlet weak var pickupNoteView: UIView!
//    @IBOutlet weak var passengerPicupLocation: UILabel!
//    @IBOutlet weak var passengerDropLocation: UILabel!
//    var userStage: JSON = [:]
//    var apiCommunicatorHelper: APICommunicator?
//    var stageService: StageService?
//    public var portraitSize: CGSize = .zero
//    enum InitialState {
//        case contracted
//        case expanded
//    }
//    
//    var initialState: InitialState = .contracted
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//      
//        initApiCommunicatorHelper()
//        doRegisterForReceivingPushNotification()
//        
//        portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
//                              height: secondPreviewView.frame.maxY)
//        self.driverImage.layer.cornerRadius = 39
//        self.driverImage.clipsToBounds = true
//        self.nameView.layer.cornerRadius = 5
//        self.pickupNoteView.layer.cornerRadius = 14
//        self.passengerDropLocation.text = destinationlocationForDriverDetailse
//        self.passengerPicupLocation.text = picuplocationForDriverDetails
//        self.passengerTotalFare.text = String(totalFareForDriverDetails)
//        manageStage()
//       
//        
//        // Do any additional setup after loading the view.
//    }
//    func initApiCommunicatorHelper() {
//        self.apiCommunicatorHelper = APICommunicator(view: self.view)
//        self.apiCommunicatorHelper?.delegate = self
//        self.stageService = StageService(self.view, communicator: apiCommunicatorHelper!)
//    }
//    
//    
//    func doRegisterForReceivingPushNotification() {
//        NotificationCenter.default.addObserver(self, selector: #selector(self.doReceivePayload(_:)), name: .pushPayload, object: nil)
//    }
//    
//    @objc func doReceivePayload(_ notification: NSNotification) {
//                    if msgAction == "accept_trip_request" {
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//                            self.driverDetails()
//                        }
//                        
//                     self.rideStageMessage.text = "Driver is arriving"
//                    
//                    }else if msgAction == "ready_to_pickup" {
//                     self.rideStageMessage.text = "Driver has arrived"
//                    }else if msgAction == "trip_start" {
//                    self.rideStageMessage.text = "On the way to destination"
//                        self.cancelRequest.isHidden = true
//                    }else if msgAction == "end_trip"{
//                     
//                    }else if msgAction == "trip_pause"{
//                        self.rideStageMessage.text = "Distination reached"
//                    }else if msgAction == "trip_resume" {
//                     self.rideStageMessage.text = "On the way to destination"
//                   }
//               }
//    deinit {
//        NotificationCenter.default.removeObserver(self)
//    }
//    
//    func driverDetails(){
//            let RequestID = Helpers.getStringValueForKey(Constants.TRIP_ID)
//        stageService?.doDriverDetailsRequest(id: Int(RequestID) ?? 00)
//    }
//
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        
//        view.layer.cornerRadius = 12
//    }
//    @IBAction func cancelDriver(_ sender: Any) {
//        (parent as? HomeController)?.cancelRequestLabel = 2
//        (parent as? HomeController)?.showCancelAlart()
//    }
//    
//    @IBAction func makePhoneCall(_ sender: Any) {
//        driverPhone.doMakeAPhoneCall()
//  
//        
//    }
//    @IBAction func makeSms(_ sender: Any) {
//        driverPhone.doSms()
//    }
//    var initialPointOffset: CGFloat {
//        switch initialState {
//        case .contracted:
//            return searchBoxContainerView?.frame.height ?? 0
//        case .expanded:
//            return pullUpControllerPreferredSize.height
//        }
//    }
//    
//    func getDriverResponse(_ data: [String: Any], statusCode: Int){
//        if statusCode == Constants.SUCCESS_STATUS_CODE {
//            let response = JSON(data)
//            print(response)
//            if !response.isEmpty {
//                if let driverFastName = response["first_name"].string{
//                    if let driverLastName = response["last_name"].string{
//                        self.driverName.text = "\(driverFastName) \(driverLastName)"
//                    }
//                }
//                if let chanel = response["channel_name"].string{
//                    driverChanelName = chanel
//                    
//                }
//                if let driverPhoneNumbe = response["mobile_no"].string{
//                    driverPhone = driverPhoneNumbe
//                }
//                if let driverId = response["driver_id"].int {
//                    Helpers.setStringValueWithKey(String(driverId), key: Constants.DRIVER_ID)
//                }
//                if let Profileurl = response["profile_pic_url_alt"].string{
//                    let urlString = Profileurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                    let url = URL(string: urlString!)
//                    self.driverImage.kf.setImage(with: url)
//                }
//                if let rating = response["driver_rating"].int{
//                    self.driverRating.text = String(rating)
//                }
//                if let totalTrip = response["driver_trip_count"].int{
//                    self.totalTrip.text = "Total trip : \(totalTrip)"
//                }
//                
//                
//                if let activeVahicale = response["active_vehicle"].dictionary{
//                    if let model = activeVahicale["model"]?.string{
//                        self.carName.text = model
//                    }
//                    if let carNumber = activeVahicale["license_plate"]?.string{
//                        self.carNumber.text = carNumber
//                    }
//                    if let vehicalType = activeVahicale["category_type"]?.string{
//                        self.driverCarType.text = vehicalType
//                    }
//                    if let catId = activeVahicale["category_id"]?.int {
//                       
//                    }
//                }
//            }
//        }
//    }
//    
//    
//    //stage manage
//    func manageStage(){
//        
//        let stageInfo = Helpers.getStringValueForKey(Constants.STAGE_DATA)
//        userStage = JSON.init(parseJSON: stageInfo)
//        
//        print(userStage)
//        if let Data = userStage["data"].dictionary{
//            if let stageData = Data["stage_data"]?.dictionary{
//                
//                
//                if let tripId = stageData["request_id"]?.int {
//                    Helpers.setStringValueWithKey(String(tripId), key: Constants.TRIP_ID)
//                }
//                if let passengerStage = stageData["passenger_stage"]?.int{
//                    print(passengerStage)
//                }
//                if let driverStage = stageData ["driver_stage"]?.int{
//                    
//                    
//                    if driverStage == 1 {
//                        
//                    }else if driverStage == 2{
//                          self.rideStageMessage.text = "Driver is arriving"
//                    }else if driverStage == 3 {
//                        
//                        self.rideStageMessage.text = "Driver arrived"
//                    }else if driverStage == 4 {
//                        
//                       self.rideStageMessage.text = "On the way to destination"
//                    }else if driverStage == 5 {
//                        
//                    }else if driverStage == 6 {
//                        
//                    }
//                }
//                if let pessengerObj = stageData["passenger_object"]?.dictionary{
//                    
//                    if let stage1 = pessengerObj["stage_1"]?.dictionary {
//                        if let dataRecive = stage1["data_received"]?.dictionary{
//                            if let destinationName = dataRecive["dest_address"]?.string{
//                                self.passengerDropLocation.text = destinationName
//                            }
//                            if let piclocation = dataRecive["location_name"]?.string{
//                                self.passengerPicupLocation.text = piclocation
//                            }
//                            if let estimateFare = dataRecive["estimated_fare"]?.string{
//                                self.passengerTotalFare.text = estimateFare
//                            }
//                            
//                            
//                            
//                        }
//                    }
//                    
//                    
//                    if let stage2_2 = pessengerObj["stage_2_2"]?.dictionary{
//                        if let dataDriverDetails = stage2_2["data_driver_details"]?.dictionary{
//                            if let code = dataDriverDetails["code"]?.int{
//                                if code == Constants.SUCCESS_STATUS_CODE {
//                                    if let driverFastName = dataDriverDetails["first_name"]?.string{
//                                        if let driverLastName = dataDriverDetails["last_name"]?.string{
//                                            self.driverName.text = "\(driverFastName) \(driverLastName)"
//                                        }
//                                    }
//                                    if let chanel = dataDriverDetails["channel_name"]?.string{
//                                        
//                                        
//                                    }
//                                    if let driverPhoneNumbe = dataDriverDetails["mobile_no"]?.string{
//                                        driverPhone = driverPhoneNumbe
//                                    }
//                                    if let driverId = dataDriverDetails["driver_id"]?.int {
//                                        Helpers.setStringValueWithKey(String(driverId), key: Constants.DRIVER_ID)
//                                    }
//                                    if let Profileurl = dataDriverDetails["profile_pic_url_alt"]?.string{
//                                        let urlString = Profileurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                                        let url = URL(string: urlString!)
//                                        self.driverImage.kf.setImage(with: url)
//                                    }
//                                    if let rating = dataDriverDetails["driver_rating"]?.int{
//                                        self.driverRating.text = String(rating)
//                                    }
//                                    if let totalTrip = dataDriverDetails["driver_trip_count"]?.int{
//                                        self.totalTrip.text = "Total trip : \(totalTrip)"
//                                    }
//                                    if let activeVahicale = dataDriverDetails["active_vehicle"]?.dictionary{
//                                        if let model = activeVahicale["model"]?.string{
//                                            self.carName.text = model
//                                        }
//                                        if let carNumber = activeVahicale["license_plate"]?.string{
//                                            self.carNumber.text = carNumber
//                                        }
//                                        if let vehicalType = activeVahicale["category_type"]?.string{
//                                            self.driverCarType.text = vehicalType
//                                        }
//                                    }
//                                    
//                                }
//                            }
//                        }
//                    }
//                    if (pessengerObj["stage_4"]?.dictionary) != nil {
//                      self.rideStageMessage.text = "On the way to destination"
//                    }
//                    if (pessengerObj["stage_4_1"]?.dictionary) != nil{
//                       self.rideStageMessage.text = "Distination reached"
//                    }
//                    if (pessengerObj["stage_4_2"]?.dictionary) != nil{
//                        self.rideStageMessage.text = "On the way to destination"
//                    }
//                    
//                }
//                
//            }
//            
//        }
//        
//    }
//    override func pullUpControllerWillMove(to stickyPoint: CGFloat) {
//        //        print("will move to \(stickyPoint)")
//    }
//    
//    override func pullUpControllerDidMove(to stickyPoint: CGFloat) {
//        //        print("did move to \(stickyPoint)")
//    }
//    
//    override func pullUpControllerDidDrag(to point: CGFloat) {
//        //        print("did drag to \(point)")
//    }
//    override var pullUpControllerPreferredSize: CGSize {
//        return portraitSize
//    }
//    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
//        switch initialState {
//        case .contracted:
//            
//            return [firstPreviewView.frame.maxY]
//        case .expanded:
//            
//            return [searchBoxContainerView.frame.maxY, firstPreviewView.frame.maxY]
//            
//        }
//    }
//    override var pullUpControllerBounceOffset: CGFloat {
//        return 20
//    }
//    
//    override func pullUpControllerAnimate(action: PullUpController.Action,
//                                          withDuration duration: TimeInterval,
//                                          animations: @escaping () -> Void,
//                                          completion: ((Bool) -> Void)?) {
//        switch action {
//        case .move:
//            UIView.animate(withDuration: 0.3,
//                           delay: 0,
//                           usingSpringWithDamping: 0.7,
//                           initialSpringVelocity: 0,
//                           options: .curveEaseInOut,
//                           animations: animations,
//                           completion: completion)
//        default:
//            UIView.animate(withDuration: 0.3,
//                           animations: animations,
//                           completion: completion)
//        }
//    }
//}
//
//extension DriverDetailsViewController: APICommunicatorDelegate {
//    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
//     if methodTag == MethodTags.TWELVE {
//            getDriverResponse(data, statusCode: statusCode)
//        }
//    }
//}
