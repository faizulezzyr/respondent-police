//
//  Pubna.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 4/2/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation
import PubNub
import CoreLocation
extension HomeController: PNObjectEventListener {
    
    
    func Animation(Lat: Double, lng: Double, oldLat: Double, OldLng: Double){
        let oldCoodinate: CLLocationCoordinate2D? = CLLocationCoordinate2D(latitude: oldLat , longitude: OldLng)
        let newCoodinate: CLLocationCoordinate2D? = CLLocationCoordinate2D(latitude: Lat , longitude: lng)
        driverMarker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
       // driverMarker.rotation = CLLocationDegrees(getHeadingForDirection(fromCoordinate: oldCoodinate!, toCoordinate: newCoodinate!))
        //found bearing value by calculation when marker add
        driverMarker.position = oldCoodinate!
        //this can be old position to make car movement to new position
        driverMarker.map = myView
        
        driverMarker.iconView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        let imageView = UIImageView(frame: CGRect(x: (driverMarker.iconView?.frame.width)!/2 - 14, y: (driverMarker.iconView?.frame.height)! - 36, width: 30, height: 30))
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        let urlString = victimeImageurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = URL(string: urlString!)
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: url, placeholder: UIImage(named: "placeImage"))
        driverMarker.iconView?.addSubview(imageView)
        driverMarker.title  = "\(victimName ?? "")"
        imageView.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0) //we need adjust anchor point to achieve the desired behavior
        imageView.layer.frame = CGRect(x: (driverMarker.iconView?.frame.width)!/2 - 14, y: (driverMarker.iconView?.frame.height)! - 36, width: 30, height: 30)
        
        //marker movement animation
        CATransaction.begin()
        CATransaction.setValue(Int(2.0), forKey: kCATransactionAnimationDuration)
        CATransaction.setCompletionBlock({() -> Void in
            self.driverMarker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
            //driverMarker.rotation = CDouble(Data.value(forKey: "bearing"))
            //New bearing value from backend after car movement is done
        })
        driverMarker.position = newCoodinate!
        //this can be new position after car moved from old position to new position with animation
        
        driverMarker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
       // driverMarker.rotation = CLLocationDegrees(getHeadingForDirection(fromCoordinate: oldCoodinate!, toCoordinate: newCoodinate!))
        //found bearing value by calculation
        zoomToCoordinates(newCoodinate!, zoom: 17)
        CATransaction.commit()
    }
    
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        
        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            
            return degree
        }
        else {
            
            return 360 + degree
            
        }
    }
    
    
    
    
    
    // Handle new message from one of channels on which client has been subscribed.
    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
        // Handle new message stored in message.data.message
        if message.data.channel != message.data.subscription {
        }
        else {
            // Message has been received on channel stored in message.data.channel.
        }
        let subscription = message.data.message
        
      
        if let subData = subscription as? [String: AnyObject] {
            print(subData)
            oldLet = newLet
            oldLng = newLng
            if let lat = subData["lat"] {
                if let lng = subData["lng"]{
                    newLet = String(describing: lat)
                    newLng = String(describing: lng)
                    Helpers.setStringValueWithKey(newLet!, key: Constants.DRIVER_LAST_LAT)
                    Helpers.setStringValueWithKey(newLng!, key: Constants.DRIVER_LAST_LNG)
                    
                    if oldLet != nil {
                        Animation(Lat: Double(newLet!)!, lng: Double(newLng!)!, oldLat: Double(oldLet!)!, OldLng: Double(oldLng!)!)
                    }
             
                }
            }
        }
    }
    // New presence event handling.
    //    func client(_ client: PubNub, didReceivePresenceEvent event: PNPresenceEventResult) {
    //
    //        // Handle presence event event.data.presenceEvent (one of: join, leave, timeout, state-change).
    //        if event.data.channel != event.data.subscription {
    //
    //            // Presence event has been received on channel group stored in event.data.subscription.
    //        }
    //        else {
    //
    //            // Presence event has been received on channel stored in event.data.channel.
    //        }
    //
    //        if event.data.presenceEvent != "state-change" {
    //
    //            print("\(event.data.presence.uuid) \"\(event.data.presenceEvent)'ed\"\n" +
    //                "at: \(event.data.presence.timetoken) on \(event.data.channel) " +
    //                "(Occupancy: \(event.data.presence.occupancy))");
    //        }
    //        else {
    //
    //            print("\(event.data.presence.uuid) changed state at: " +
    //                "\(event.data.presence.timetoken) on \(event.data.channel) to:\n" +
    //                "\(event.data.presence.state)");
    //        }
    //    }
    // Handle subscription status change.
    //    func client(_ client: PubNub, didReceive status: PNStatus) {
    //
    //        if status.operation == .subscribeOperation {
    //
    //            // Check whether received information about successful subscription or restore.
    //            if status.category == .PNConnectedCategory || status.category == .PNReconnectedCategory {
    //
    //                let subscribeStatus: PNSubscribeStatus = status as! PNSubscribeStatus
    //                if subscribeStatus.category == .PNConnectedCategory {
    //
    //                    // This is expected for a subscribe, this means there is no error or issue whatsoever.
    //
    //                    // Select last object from list of channels and send message to it.
    //                    let targetChannel = client.channels().last!
    //                    client.publish("Hello from Ezzyr", toChannel: targetChannel,
    //                                   compressed: false, withCompletion: { (publishStatus) -> Void in
    //
    //                                    if !publishStatus.isError {
    //
    //                                        // Message successfully published to specified channel.
    //                                    }
    //                                    else {
    //
    //                                        /**
    //                                         Handle message publish error. Check 'category' property to find out
    //                                         possible reason because of which request did fail.
    //                                         Review 'errorData' property (which has PNErrorData data type) of status
    //                                         object to get additional information about issue.
    //
    //                                         Request can be resent using: publishStatus.retry()
    //                                         */
    //                                    }
    //                    })
    //                }
    //                else {
    //
    //                    /**
    //                     This usually occurs if subscribe temporarily fails but reconnects. This means there was
    //                     an error but there is no longer any issue.
    //                     */
    //                }
    //            }
    //            else if status.category == .PNUnexpectedDisconnectCategory {
    //
    //                /**
    //                 This is usually an issue with the internet connection, this is an error, handle
    //                 appropriately retry will be called automatically.
    //                 */
    //            }
    //                // Looks like some kind of issues happened while client tried to subscribe or disconnected from
    //                // network.
    //            else {
    //
    //                let errorStatus: PNErrorStatus = status as! PNErrorStatus
    //                if errorStatus.category == .PNAccessDeniedCategory {
    //
    //                    /**
    //                     This means that PAM does allow this client to subscribe to this channel and channel group
    //                     configuration. This is another explicit error.
    //                     */
    //                }
    //                else {
    //
    //                    /**
    //                     More errors can be directly specified by creating explicit cases for other error categories
    //                     of `PNStatusCategory` such as: `PNDecryptionErrorCategory`,
    //                     `PNMalformedFilterExpressionCategory`, `PNMalformedResponseCategory`, `PNTimeoutCategory`
    //                     or `PNNetworkIssuesCategory`
    //                     */
    //                }
    //            }
    //        }
    //    }
}





