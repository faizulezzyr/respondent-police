//
//  extentionHome.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 24/12/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMaps
import CoreLocation
import GooglePlaces
import Alamofire
import Kingfisher
import Lottie



extension HomeController {
   
    func getAddressForLiveLocation(Location: CLLocationCoordinate2D?){
        geocoder.reverseGeocodeCoordinate(Location!)  { response, error in
            if error != nil {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            } else {
                if let places = response?.results() {
                    if let place = places.first {
                        if let lines = place.lines {
                            self.myCurrentLocationName = lines[0]
                        }
                    } else {
                        print("GEOCODE: nil first in places")
                    }
                } else {
                    print("GEOCODE: nil in places")
                }
            }
        }
    }
    
    
    
    func getOnlieStursResponse(_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            print(response)
              if !response.isEmpty {
                if let currentStatus = response["current_status"].string{
                   onlineStatus = currentStatus
                }
                
             }
        }
    }
    
    //MARK:- Api implementation for Direction
    
    
    
    func getDirectionResponse (_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
        
            if !response.isEmpty {
                let routes = response["routes"].arrayValue
                
                if let distance = routes[0]["legs"][0]["distance"].dictionary {
                    print(distance)
                    if let meters = distance["value"]?.int {
                        calculatedDistance = Double(meters) / 1000.0
                        print(calculatedDistance)
                    }
                }
                if let time = routes[0]["legs"][0]["duration"].dictionary{
                    if let sec = time["value"]?.int {
                        calculateMinute = Double(sec / 60)
                    }
                }
                for route in routes {
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    let path = GMSPath.init(fromEncodedPath: points!)
                    let bounds = GMSCoordinateBounds(path: path!)
                    let polyline = GMSPolyline(path: path)
                    polyline.strokeColor = .black
                    polyline.strokeWidth = 3.0
                    self.myView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                    polyline.map = self.myView
                }
            }
        }
    }
    
    
    

    
    func profileInfoSetup(){
        let userinfo = Helpers.getStringValueForKey(Constants.PROFILE_INFO)
        let userInfoJSON = JSON.init(parseJSON: userinfo)
        print(userInfoJSON)
        if !userInfoJSON.isEmpty {
            if let profile_picture_url = userInfoJSON["profile_pic_url"].string {
                let urlString = profile_picture_url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                let url = URL(string: urlString!)
                userImage.kf.indicatorType = .activity
                userImage.kf.setImage(with: url, placeholder: UIImage(named: "placeImage"))
            }
            else {
                victimeImage.image = UIImage(named: "placeImage")
            }
            var fullName = ""
            if let first_name = userInfoJSON["first_name"].string {
                fullName = first_name
            }
            if let last_name = userInfoJSON["last_name"].string {
                if fullName.count > 0 {
                    fullName = fullName + " " + last_name
                }
            }
            userName.text = fullName.uppercased()
            if let activeVehicle =  userInfoJSON["vehicle_id"].int {
               
                vehicleID = activeVehicle
            }
            if let vehicleCatagoriId = userInfoJSON["vehicle_type"].int{
                vehicleCatID = vehicleCatagoriId
            }
            if let mobileNumber = userInfoJSON["mobile_no"].string {
                driverPhone = mobileNumber
            }
            if let companyData = userInfoJSON["company_data"].dictionary{
                if let compId = companyData["company_id"]?.int{
                    companyID = compId
                }
            }
            if let bpRank = userInfoJSON["bpid"].string{
                self.bpId.text = bpRank
            }
            if let Brank = userInfoJSON["bp_rank"].string{
                self.rank.text = Brank
            }
            if let policeStation = userInfoJSON["company_data"].dictionary{
                if let policeS = policeStation["company_name"]?.string{
                    self.policeStation.text = "\(policeS) police station"
                }
            }
            
            if let onlineStatus = userInfoJSON["driver_online_status"].string{
                if onlineStatus == "Online"  {
                   self.mySwithc.setOn(true, animated: false)
                    self.mySwithc.onTintColor = Commons.colorWithHexString("1B9900")
                    self.mySwithc.thumbTintColor = Commons.colorWithHexString("1C27C0")
                }else{
                   
                }
            }
        }
    }
    
    func getUpdateGpsResponse(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            print(response)
            
        }
    }
    func getNotifyRescueResponse(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            print(response)
            goNuMode()
            
        }
    }
    func getNutralModeResponse(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            print(response)
            if !response.isEmpty {
                if let successMsg = response["success"].bool{
                    if successMsg == true{
                        homeBack.isHidden = false
                        homeBackView.isHidden = false
                        driverProfile.isHidden = true
                        myView.isHidden = true
                        isInaCase = 0
                       // getProfile()
                        
                    }else{
                        
                    }
                }
                
            }
        }
    }
    /// User profile respond
    
    func getProfileResponse(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            print(response)
            if !response.isEmpty {
                if loginTace == 0 {
                    if let stageData = response["stage_data"].dictionary {
                        if let driverStage = stageData["driver_stage"]?.int {
                            if driverStage != 101 && driverStage != 0 {
                                requestForStage()
                            }else {
                            }
                        }
                    }
                }else{
                }
                if let profileInfo = response.rawString() {
                    Helpers.removeValue(Constants.PROFILE_INFO)
                    Helpers.setStringValueWithKey(profileInfo, key: Constants.PROFILE_INFO)
                    profileInfoSetup()
                }
            }
        }else {
            
            Helpers.removeValue(Constants.PROFILE_INFO)
            Helpers.removeValue(Constants.USER_ID)
            UIApplication.shared.keyWindow?.rootViewController = initViewController()
        }
    }
    // User Stage Responds
    func getUserStageDataResponse(_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
          
            if !response.isEmpty {
                if let stageData = response.rawString() {
                    Helpers.removeValue(Constants.STAGE_DATA)
                    Helpers.setStringValueWithKey(stageData, key: Constants.STAGE_DATA)
                   if let Data = response["data"].dictionary{
                              if let stageData = Data["stage_data"]?.dictionary{
                                  
                                  
                                  if let tripId = stageData["request_id"]?.int {
                                      Helpers.setStringValueWithKey(String(tripId), key: Constants.TRIP_ID)
                                  }
                                
                                  if let driverStage = stageData ["driver_stage"]?.int{
                                      self.StaticDriverStage = driverStage
                                      if driverStage == 1 {
                                        self.driverProfile.isHidden = false
                                          self.homeBack.isHidden = true
                                          self.homeBackView.isHidden = true
                                      }else if driverStage == 2{
                                          self.driverProfile.isHidden = false
                                          self.homeBack.isHidden = true
                                          self.homeBackView.isHidden = true

                                      }else if driverStage == 3 {
                   
                                      }
                                  }
                                  if let pessengerObj = stageData["driver_object"]?.dictionary{
                                      if let stage1 = pessengerObj["stage_1"]?.dictionary {
                                          if let dataRecive = stage1["data_received"]?.dictionary{
                                              if StaticDriverStage != 101 && StaticDriverStage != 0{
                                                  if let destLat = dataRecive["dest_latitude"]?.string{
                                                      if let destLng = dataRecive["dest_longitude"]?.string{
                                                          self.origin = "\(destLat),\(destLng)"
                                                          
                                                      }
                                                  }
                                                  if let picLat = dataRecive["latitude"]?.string{
                                                      if let picLng = dataRecive["longitude"]?.string{
                                                          self.destination = "\(picLat),\(picLng)"
                                                          DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                                              self.directionService?.getRoutes(self.origin, destination: self.destination)
                                                          }
                                                      }
                                                  }
                                              }
                                              
                                              
                                          }
                                      }
                                      if let stage1_2 = pessengerObj["stage_1_2"]?.dictionary{
                                          if let data_push_msg = stage1_2["data_response"]?.dictionary{
                                              print(data_push_msg)
                                              if let victimeN = data_push_msg["passenger_name"]?.string{
                                                  self.victimeName.text = victimeN
                                                  victimName = victimeN
                                              }
                                              if let emergencyContactName = data_push_msg["emg_name"]?.string{
                                                  self.victimeEmargencyContact.text = "Emergency contact : \(emergencyContactName)"
                                              }
                                              if let victimeNumb = data_push_msg["passenger_mobile"]?.string{
                                                  self.victimeNumber = victimeNumb
                                                 pubInit(Chanal: "chnl_dc_\(victimeNumber)")
                                              }
                                              if let emergincyContacNumber = data_push_msg["emg_mobile_no"]?.string{
                                                  self.victimeEmergencyNumber = emergincyContacNumber
                                              }
                                              if let victimeImageUrl = data_push_msg["passenger_picture"]?.string{
                                                  victimeImageurl = victimeImageUrl
                                                  let urlString = victimeImageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                                  let url = URL(string: urlString!)
                                                  victimeImage.kf.indicatorType = .activity
                                                  victimeImage.kf.setImage(with: url, placeholder: UIImage(named: "placeImage"))
                                              }
                                              if let passengerID = data_push_msg["passenger_id"]?.int{
                                                  victimeID = String(passengerID)
                                              }
                                              if let currentLat = data_push_msg["cur_lat"]?.string{
                                                  if let currentLng = data_push_msg["cur_lng"]?.string{
                                                   setMarkerAndGetDirection(lat: currentLat, lng: currentLng)
                                                  }
                                              }
                                          }
                                      }
                                     
                                
                                  }
                                  
                              }
                              
                          }
                }
            }
        }
    }
    
    // fake car responds

}
