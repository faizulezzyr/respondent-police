//
//  HomeController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 11/11/18.
//  Copyright � 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMaps
import CoreLocation
import GooglePlaces
import Alamofire
import Kingfisher
import Lottie
import PubNub
import GoogleUtilities
import LSDialogViewController
public var driverPhone = ""
var isOpen = false
var vahicleGroupId = 0
var BackStage = 0
var vehicleCatID: Int?
var companyID: Int?
var vehicleID: Int?
var myCurrentLocation: CLLocationCoordinate2D?
var markerIconName = ""
var onlineStatus = "1"
class HomeController: BaseController,GMSMapViewDelegate {
    //Fare details Outlet

    //// other outlet
    @IBOutlet weak var anView: UIView!
    @IBOutlet weak var driverProfile: UIView!
    
    @IBOutlet var myView: GMSMapView!
    @IBOutlet weak var homeBack: UIImageView!
    @IBOutlet weak var homeBackView: UIView!
    
    @IBOutlet weak var internetErrorView: UIView!
    @IBOutlet weak var internetErrorLabel: UILabel!
    @IBOutlet weak var mbutton: UIButton!
  
   
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var victimeImage: UIImageView!
    @IBOutlet weak var victimeName: UILabel!
    @IBOutlet weak var victimeEmargencyContact: UILabel!
    @IBOutlet weak var mySwithc: UISwitch!
    

    
    @IBOutlet weak var bpId: UILabel!
    @IBOutlet weak var rank: UILabel!
    @IBOutlet weak var policeStation: UILabel!
    
    
    var pickupLocation = PickupLocationModel()
    var dropLocation = DropupLocationModel()
    var calculatedDistance: Double = 0.0
    var calculateMinute: Double = 0.0

    var memberId = 0
    var userService: UserService?
    var apiCommunicatorHelper: APICommunicator?
    var settingsService: SettingsService?
    var directionService: DirectionService?
    var placeSearchService: PlaceSearchService?
    var bookingService: BookingService?
    var stageService: StageService?
    var fakeCar : FakeCarService?
    var currentLocation: CLLocationCoordinate2D? = nil
    var locationManager: CLLocationManager!
    var currentLocationMarker: GMSMarker?
    var firstTimeLocation = true
    var topBack = 0
    var origin = ""
    var destination = ""

    let geocoder = GMSGeocoder()
    var markLet: Double?
    var markLng: Double?
    var pickUpCoordinate: CLLocationCoordinate2D?
    var destinationCoordinate: CLLocationCoordinate2D?
    var userStage: JSON = [:]
  
   
    var myTimer = Timer()
    var StaticDriverStage = 0
    var cancelRequestLabel = 1
    var notificationImage = "bell"
    var client : PubNub?
    var config : PNConfiguration?
    var driverMarker = GMSMarker()
    var newLet : String?
    var newLng : String?
    var oldLet : String?
    var oldLng : String?
    let logo = UIImage(named: "ezzyrbuttonsolid2")!
    var isStageData: Bool = false
    var autoCompleateLocationCordinate: CLLocationCoordinate2D?
    var driverSearchLottiAnimation: AnimationView?
    var victimeNumber: String = ""
    var victimeEmergencyNumber: String = ""
    var NumberOfAlart = 0

    var myCurrentLocationName = ""
    var victimeImageurl = ""

    // variable for picup and drop

    
    var homeApiRequest = 0
    var isSwithcOn = false
    var isSetuPPickupAndDestination = false
    let dialogViewController = ErrorView(nibName: "serverErrorMsg", bundle: nil)
    let NotFoundDialog = driverNotFoundView(nibName: "DriverNotFound", bundle: nil)


    var isStartRequestProcess = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initGester()
        initApiCommunicatorHelper()
        getProfile()
        doRegisterForReceivingPushNotification()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        mbutton.addTarget(self, action: #selector(showAndHide), for: .touchUpInside)
        initGoogleMap()
        initializeOtherView()
      
     

    }

    @objc func showAndHide(){
        
        if(!isOpen)
            
        {
            isOpen = true
            
            let menuVC : SideBarController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! SideBarController
            self.view.addSubview(menuVC.view)
            self.addChild(menuVC)
            menuVC.view.layoutIfNeeded()
            
            menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width+100, height: UIScreen.main.bounds.size.height);
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            }, completion:nil)
            
        }else if(isOpen)
        {
            isOpen = false
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
                
            })
        }
    }

     func showDialog(_ animationPattern: LSAnimationPattern) {
        let dialogViewController = SuccessView(nibName: "BookingSuccess", bundle: nil)
        dialogViewController.delegate = self
//        dialogViewController.text.text = "Your schedule trip is placed successfully. You will find the details of your trip on upcoming trip list."
        presentDialogViewController(dialogViewController, animationPattern: animationPattern)
    }
    func dismissDialog() {
        dismissDialogViewController(LSAnimationPattern.fadeInOut)
    }
    func pubInit(Chanal: String) {
        config = PNConfiguration(publishKey: Constants.PUBNUB_PUBLISH_KEY, subscribeKey: Constants.PUBNUB_SUB_KEY)
        client = PubNub.clientWithConfiguration(config!)
        client!.subscribeToChannels([Chanal], withPresence: false)
        client!.publish("Respondent+PubNub!", toChannel: Chanal, compressed: false, withCompletion: nil)
        client!.addListener(self)
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
    }

    


    func driverDetails(){
        let RequestID = Helpers.getStringValueForKey(Constants.TRIP_ID)
        stageService?.doDriverDetailsRequest(id: Int(RequestID) ?? 00)
    }
    func addDriveInfo() {

        driverProfile.isHidden = false
        victimeName.text = victimName
        
    }
    func removerDriverInfo(){
        driverProfile.isHidden = true
    }


    
    func doRegisterForReceivingPushNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.doReceivePayload(_:)), name: .pushPayload, object: nil)
    }
    
    @objc func doReceivePayload(_ notification: NSNotification) {
        if let info = notification.userInfo as? [String: Any] {
            if let msg = info["message"] as? String {
                let message = JSON.init(parseJSON: msg)
                print(message)
                if let action = message["action"].string {
                    if action == "trip_request" {
                        pubInit(Chanal: driverChanelName)
                        if let submsg = message["msg"].dictionary {
                            if let victimeN = submsg["passenger_name"]?.string {
                                victimName = victimeN
                            }
                            if let victimeLet = submsg["pick_up_lat"]?.string{
                                victimLat = victimeLet
                            }else if let victimLa = submsg["pick_up_lat"]?.double{
                                 victimLat = String(victimLa)
                            }
                            if let victimLong = submsg["pick_up_lng"]?.string{
                                 victimLng = victimLong
                            }else if let victimL = submsg["pick_up_lng"]?.double{
                                 victimLng = String(victimL)
                            }
                            if let victimAdd = submsg["pick_up_location"]?.string{
                                victimeAddress = victimAdd
                            }
                            if let reqId = submsg["request_id"]?.int {
                                Helpers.setIntValueWithKey(reqId, key: Constants.TRIP_REQUEST_ID)
                            }
                            if let victimeId = submsg["passenger_id"]?.string{
                               victimeID = victimeId
                            }else if let vicId = submsg["passenger_id"]?.int{
                               victimeID = String(vicId)
                            }
                              navigateincoming()
                        }
                       // driverMarker.position = cordinate
                        
                    }else if action == "ready_to_pickup" {
                        self.removerDriverInfo()
                    }else if action == "trip_start" {
                    }else if action == "end_trip"{
                     self.removerDriverInfo()
                      navigateToFareAndRating()
                    }else if action == "cancel_trip_request" {
                        hideTop()
                        removerDriverInfo()
                    }else if action == "new_content_notification"{
                        var noti =  Helpers.getIntValueForKey(Constants.TOTAL_NOTIFICATION)
                        if noti == -1 {
                            noti = 0
                        }
                        let NotiPluse = Int(noti) + 1
                        Helpers.removeValue(Constants.TOTAL_NOTIFICATION)
                        Helpers.setIntValueWithKey(NotiPluse, key: Constants.TOTAL_NOTIFICATION)
                    }else if action == "driver_appply" {
                        
                    }
                }
            }
        }
    }
    

    
    func initGester(){
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(moveToNextItem(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(moveToNextItem(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }
    @objc func moveToNextItem(_ sender:UISwipeGestureRecognizer) {
        switch sender.direction{
        case .left:
         
            if BackStage == 0 {
                if isStageData == false{
                   showAndHide()
                }
            }
            
        case .right:
          
            if BackStage == 0 {
                if isStageData == false{
                    showAndHide()
                }
            }
           
        default: break //default
        }
        
    }

    func initializeOtherView(){
        if isInaCase == 1 {
            self.homeBack.isHidden = true
            self.homeBackView.isHidden = true
            onlineStatus = "1"
 
            setMarkerAndGetDirection(lat: victimLat!, lng: victimLng!)
        }

        Commons.makeCardView(view: homeBackView)
        userImage.layer.cornerRadius = 60
        userImage.clipsToBounds = true
        victimeImage.layer.cornerRadius = 30
        victimeImage.clipsToBounds = true
        self.anView.isHidden = true

        self.internetErrorView.isHidden = true
        dialogViewController.homeDelegate = self
        NotFoundDialog.homeViewDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.tryAgainNoti), name: NSNotification.Name(rawValue: "serverError"), object: nil)
        driverProfile.isHidden = true

    }
    func setMarkerAndGetDirection(lat: String, lng: String){
        destination = lat + "," + lng
        origin = "\(myCurrentLocation?.latitude ?? 0.0)" + "," + "\(myCurrentLocation?.longitude ?? 0.0)"
        
        self.directionService?.getRoutes(self.origin, destination: self.destination)
       // marker(lat: Double(lat) as! Double, lng: Double(lng) as! Double, name: "", widht: 20, height: 40, titleString: "\(victimName ?? "")")
    }
    func marker(lat: Double, lng: Double, name: String, widht: Double, height: Double, titleString: String){
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 16.0)
        myView.camera = camera
        let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let marker = GMSMarker(position: position)
        let image =  UIImage(named: name)
       // marker.icon =  self.imageWithImage(image: image!, scaledToSize: CGSize(width: widht, height: height))
        marker.title = titleString
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.map = myView
        self.myView.selectedMarker = marker
    }
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }

    @objc func tryAgainNoti(notif: NSNotification) {
        if  NumberOfAlart == 0{
            showInernetDialog(.slideBottomTop)
        }
    }
  
    func showInernetDialog(_ animationPattern: LSAnimationPattern) {
        presentDialogViewController(dialogViewController, animationPattern: animationPattern)
        globalViewControllerString = "HomeController"
        print(NumberOfAlart)
         NumberOfAlart += 1
    
    }
   
    
    func showNotFoundDailog(_ animationPattern: LSAnimationPattern) {
        presentDialogViewController(NotFoundDialog, animationPattern: animationPattern)
    }
    
    func dismissInternetDialog() {
       NumberOfAlart = 0
        switch homeApiRequest {
        case 1:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
          
            
        case 2:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
            
            
        case 3:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
        
        case 4:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
          
            
        case 5:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
       
           
        case 6:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
          
           
        case 7:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
           // getProfile()
            
        default:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
        }
    
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: .pushPayload , object: nil)
        NumberOfAlart = 0
    }
    func navigateToFareAndRating() {
        let main: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let ratingView = main.instantiateViewController(withIdentifier: "FareAndRating") as? FareAndSubmissionViewController
        let navigationController = UINavigationController(rootViewController: ratingView!)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    func navigateincoming() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let homeController = mainStoryboard.instantiateViewController(withIdentifier: "incoming") as? incomingRequestController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [homeController!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
        
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
      
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.setNavigationBarHidden(false, animated: animated)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if CLLocationManager.locationServicesEnabled() {
            startMonitoringLocation()
            addCurrentLocationMarker(name: "self_location")
        }
    }
    
    func initApiCommunicatorHelper() {
        self.apiCommunicatorHelper = APICommunicator(view: self.view)
        self.apiCommunicatorHelper?.delegate = self
        self.userService = UserService(self.view, communicator: apiCommunicatorHelper!)
        self.settingsService = SettingsService(self.view, communicator: apiCommunicatorHelper!)
        self.directionService = DirectionService(self.view, communicator: apiCommunicatorHelper!)
        self.placeSearchService = PlaceSearchService(self.view, communicator: apiCommunicatorHelper!)
        self.bookingService = BookingService(self.view, communicator: apiCommunicatorHelper!)
        self.stageService = StageService(self.view, communicator: apiCommunicatorHelper!)
        self.fakeCar = FakeCarService(self.view, communicator: apiCommunicatorHelper!)
    }
    
    func initGoogleMap(){
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.myView.isMyLocationEnabled = false
        self.myView.delegate = self
        var region:GMSVisibleRegion = GMSVisibleRegion()
        region.nearLeft = CLLocationCoordinate2DMake(26.633914, 92.6801153)
        region.farRight = CLLocationCoordinate2DMake(20.3794,88.00861410000002)
        let bounds = GMSCoordinateBounds(coordinate: region.nearLeft,coordinate: region.farRight)
        let camera = myView.camera(for: bounds, insets:UIEdgeInsets.zero)
         myView.camera = camera!;
       
        /// var mycamera = GMSCameraPosition.camera(withTarget: bounds, zoom: 6)
        
    }
    
    /// Initialize imageview and constraint


    //MARK: API Implementation
    
    func getProfile() {
        homeApiRequest = 7
        memberId = Helpers.getIntValueForKey(Constants.MEMBER_ID)
        userService?.getProfile(memberId)
    }
    
    @objc func requestForStage(){
        let userId = Helpers.getIntValueForKey(Constants.USER_ID)
        let params = [
            "driver_id" : userId
        ]
        self.stageService?.doStageRequest(params)
        
    }
    
    func PostOnlineOfline(status: String){
        let userId = Helpers.getIntValueForKey(Constants.USER_ID)
        let params = [
            "driver_id" : String(userId),
            "value" : status
        ]
        self.userService?.postOnlineStatus(params)
        print(params)
    }
    func updateGpsCordinate(){
        let userId = Helpers.getIntValueForKey(Constants.USER_ID)
        let params = [
            "driver_id": String(userId),
            "vehicle_type":  vehicleCatID ?? "",
            "latitude":  myCurrentLocation?.latitude ?? "",
            "longitude":  myCurrentLocation?.longitude ?? "",
            "altitude": "0.93",
            "location_name": myCurrentLocationName,
            "request_id": "",
            "data_set": "",
            "company_id": "\(companyID ?? 00)"

            ] as [String : Any]
        userService?.updateGpsCordinate(params)
        print(params)
        
    }
    func initViewController() -> UINavigationController {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let splashController = mainStoryboard.instantiateViewController(withIdentifier: "SplashScreenVC") as? SplashScreenController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [splashController!]
        return navigationController
    }

    @IBAction func victimeContact(_ sender: Any) {
        victimeNumber.doMakeAPhoneCall()
    }
    
 
    @IBAction func emergencyContact(_ sender: Any) {
        victimeEmergencyNumber.doMakeAPhoneCall()
    }
    
    @IBAction func rescueConfirm(_ sender: Any) {
        doRescue()
    }
    
    func doRescue(){
         let userId = Helpers.getIntValueForKey(Constants.USER_ID)
         let RequestID = Helpers.getIntValueForKey(Constants.TRIP_REQUEST_ID)
        
         let params = [
        
            "driver_id" : userId,
            "passenger_id" : "\(victimeID ?? "")" ,
            "request_id" : RequestID,
            "latitude" : "\(myCurrentLocation?.latitude ?? 0.0 )",
            "longitude" : "\(myCurrentLocation?.longitude ?? 0.0)",
        
            ] as [String : Any]
        print(params)
        
        bookingService?.doNotifyRescue(_params: params)
    }
    func goNuMode(){
        let userId = Helpers.getIntValueForKey(Constants.USER_ID)
        let RequestID = Helpers.getIntValueForKey(Constants.TRIP_REQUEST_ID)
         let params = [
            "request_level": 2,
            "driver_id": userId,
            "request_id": RequestID,
            "reason": "Compleate",
            "complete": "1",
            "pickup_duration" : "12",
            "latitude" : "\(myCurrentLocation?.latitude ?? 0.0 )",
            "longitude": "\(myCurrentLocation?.longitude ?? 0.0)",
        
            ] as [String : Any]
        print(params)
        bookingService?.doCancelRequest(params)
    }
    
    @IBAction func activeStatus(_ sender: UISwitch) {
        switch sender.isOn {
        case true:
            sender.onTintColor = Commons.colorWithHexString("1B9900")
            sender.thumbTintColor = Commons.colorWithHexString("1C27C0")
           PostOnlineOfline(status: "0")
            
        case false:
             PostOnlineOfline(status: "1")
            
        }
    }
    

    
    func hideTop(){
        mbutton.isHidden = true
  
    }
    func showTop(){
        mbutton.isHidden = false
       
    }

    let logoImage: UIImageView = {
        let myImage = UIImageView()
        myImage.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        myImage.image = UIImage(named: "ic_trip_request")
        myImage.clipsToBounds = true
        myImage.translatesAutoresizingMaskIntoConstraints = false
        
        return myImage
    }()



    func ManageHomePageViewAgain(){
        initializeOtherView()
        self.firstTimeLocation = true

        isSetuPPickupAndDestination = false
        self.pickUpCoordinate = nil
        self.destinationCoordinate = nil
        self.zoomToCoordinates(currentLocation!, zoom: 16)
        self.origin = ""
        self.destination = ""
        self.anView.isHidden = true

        self.myView.clear()
        self.myView.reloadInputViews()
        //super.viewDidLoad()
        self.locationManager.startUpdatingLocation()
        startMonitoringLocation()
        addCurrentLocationMarker(name: "self_location")

    }

    func loadUserZone(_ lat: String, lon: String) {
        placeSearchService?.getUserZone(lat, lon: lon)
    }

 }




extension HomeController: APICommunicatorDelegate {
    
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
            getProfileResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.ELEVENTH {
            getUserStageDataResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.SECOND {
            //getUserZoneResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.THIRD {
            getDirectionResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.FIFTH {
            getOnlieStursResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.SIXTH {
            getUpdateGpsResponse(data, statusCode: statusCode)
            
        }else if methodTag == MethodTags.SEVENTH {
             getNotifyRescueResponse(data, statusCode: statusCode)
        }else if methodTag == MethodTags.EIGHTH{
              getNutralModeResponse(data, statusCode: statusCode)
        }
  
    }
}





