//
//  EditProfileController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 13/11/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import Photos
import LSDialogViewController
class EditProfileController: UIViewController,UIScrollViewDelegate,UIPickerViewDelegate , UIPickerViewDataSource, UIPopoverPresentationControllerDelegate {

    @IBAction func tapToUploadImage(_ sender: Any) {
         uploadPictureActionSheet()
    }

    @IBAction func goBack(_ sender: UIButton) {
        navaigationToHome()
        
    }
    func navaigationToHome() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let homeController = mainStoryboard.instantiateViewController(withIdentifier: "HomeV") as? HomeController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [homeController!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        
        if selectedRadioButton != -1 {

            var gender = ""
            if selectedRadioButton == 1 {
                gender = MALE
            }
            else if selectedRadioButton == 0 {
                gender = FEMALE
            }
            else if selectedRadioButton == 2 {
                gender = OTHERS
            }
//            doEditProfile(first_name, last_name: last_name, user_email: user_email, dateOfBirth: dateOfBirth, gender: gender, bloodGroup: bloodGroup)
        }
    }
    

    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var profilePictureImageView: UIImageView!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var changePasswordField: UILabel!
    @IBOutlet weak var mainViewBack: UIView!
    @IBOutlet weak var Rank: UITextField!
    @IBOutlet weak var phoneN: UITextField!
    
    
    @IBOutlet weak var policeStation: UITextField!
    
    @IBOutlet weak var email: UITextField!
    
    
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var save: UIButton!
    
    
    var currUploadedPhoto = UIImage()
    let imagePicker = UIImagePickerController()
    let piker = UIDatePicker()
    let customPiker = UIPickerView()
    
    var data =  ["A+","A-","B+","B-","O+","O-","AB+","AB-","I don't know"]
    var userJSON: JSON = [:]
    var phoneNumber = ""
    
    let checkedImage = UIImage(named: "ic_radio_button_checked")
    let uncheckedImage = UIImage(named: "ic_radio_button_unchecked_black")
    let placeholder = UIImage(named: "ic_avatar")
    
    var selectedRadioButton = -1
    let MALE = "1"
    let FEMALE = "0"
    let OTHERS = "2"
    var editProfieApiRequest = 1
    var userService: UserService?
    var apiCommunicatorHelper: APICommunicator?
    var authenticationService: AuthenticationService?
    let dialogViewController = ErrorView(nibName: "serverErrorMsg", bundle: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        dialogViewController.editProfileDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.tryAgainNoti), name: NSNotification.Name(rawValue: "serverError"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.tryAgainNoti), name: NSNotification.Name(rawValue: "noInternet"), object: nil)
        initSetup()
        populateProfileInfo()
        initApiCommunicatorHelper()

        profilePictureImageView.clipsToBounds = true
        profilePictureImageView.translatesAutoresizingMaskIntoConstraints = false
        imagePicker.delegate = self
        save.layer.cornerRadius = 25
        save.clipsToBounds = false
         email.setBottomBorder(Colors.WHITE_COLOR)
          email.setBottomBorder(Colors.WHITE_COLOR)
         Rank.setBottomBorder(Colors.WHITE_COLOR)
         policeStation.setBottomBorder(Colors.WHITE_COLOR)
         phoneN.setBottomBorder(Colors.WHITE_COLOR)
        piker.maximumDate = Date()
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(profileTapped(tapGestureRecognizer:)))
        profilePictureImageView.isUserInteractionEnabled = true
        profilePictureImageView.addGestureRecognizer(tapImage)
    }
    @objc func profileTapped(tapGestureRecognizer: UITapGestureRecognizer) {
       uploadPictureActionSheet()
    }
    
    @objc func tryAgainNoti(notif: NSNotification) {
        showInernetDialog(.slideBottomTop)
    }
    
    func showInernetDialog(_ animationPattern: LSAnimationPattern) {
        presentDialogViewController(dialogViewController, animationPattern: animationPattern)
        globalViewControllerString = "EditProfileController"
    }
    func dismissInternetDialog() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.30) {
            if self.editProfieApiRequest == 1{
                self.dismissDialogViewController(LSAnimationPattern.fadeInOut)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                      self.showAlertWithTwoTextFields()
                }
              
            }else if self.editProfieApiRequest == 2{
                self.dismissDialogViewController(LSAnimationPattern.fadeInOut)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                      self.saveButtonPressed(self.save)
                }
              
            }else {
                self.dismissDialogViewController(LSAnimationPattern.fadeInOut)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.uploadPictureActionSheet()
                }
                
            }
        }
    }
    func showAlertWithTwoTextFields() {
        
        let alertController = UIAlertController(title: "", message: "Reset your password", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Reset", style: .default, handler: {
            alert -> Void in
            
            let eventNameTextField = alertController.textFields![0] as UITextField
            let descriptionTextField = alertController.textFields![1] as UITextField
            
            //print("firstName \(String(describing: eventNameTextField.text)), secondName \(String(describing: descriptionTextField.text))")
            
            if eventNameTextField.text != "" || descriptionTextField.text != ""{
                self.doChangePassword(currentPass: eventNameTextField.text!, newPass: descriptionTextField.text!, oldPass: descriptionTextField.text!)
            }else{
                let popup = CustomAlertPopup.customOkayPopup("", msgBody: Constants.MISSING_MANDATORY_FIELD)
                self.present(popup, animated: true, completion: nil)
                // Show Alert Message to User As per you want
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter current password"
            let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
            textField.addConstraint(heightConstraint)
            
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter new password"
            let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height , relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
            textField.addConstraint(heightConstraint)
            
        }
        
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        userService = UserService(self.view, communicator: apiCommunicatorHelper!)
        authenticationService = AuthenticationService(self.view, communicator: apiCommunicatorHelper!)
    }
    
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    func uploadPictureActionSheet() {
        
        let sheet = UIAlertController(title: "Upload Picture", message: nil, preferredStyle: .actionSheet)
        
        let takePhotoAction = UIAlertAction(title: "Take Photo", style: .default) { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.imagePicker.cameraCaptureMode = .photo
                self.imagePicker.modalPresentationStyle = .fullScreen
                self.present(self.imagePicker,animated: true,completion: nil)
            }
            else{
                let popup = CustomAlertPopup.customOkayPopup("No Camera", msgBody: "This device has no camera!!!")
                self.present(popup, animated: true, completion: nil)
            }
        }
        
        let photoLibraryAction = UIAlertAction(title: "Choose from Gallery", style: .default) { (action) in
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        sheet.addAction(takePhotoAction)
        sheet.addAction(photoLibraryAction)
        sheet.addAction(cancelAction)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            sheet.popoverPresentationController?.sourceView = self.view
            sheet.popoverPresentationController?.permittedArrowDirections = []
            sheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.height - 140, width: 0, height: 0)
            self.present(sheet, animated: true, completion: nil)
        }
        else {
            self.present(sheet, animated: true, completion: nil)
        }
    }
    
    @objc func changePasswordTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        //print("change password tapped")
        showAlertWithTwoTextFields()
    }
    
    
    @objc func profileImageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        uploadPictureActionSheet()
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
    
    func initSetup() {
        
        let tapChangePassword = UITapGestureRecognizer(target: self, action: #selector(changePasswordTapped(tapGestureRecognizer:)))
        changePasswordField.isUserInteractionEnabled = false
        changePasswordField.addGestureRecognizer(tapChangePassword)
        
        let tapProfileImage = UITapGestureRecognizer(target: self, action: #selector(profileImageTapped(tapGestureRecognizer:)))
        profilePictureImageView.isUserInteractionEnabled = true
        profilePictureImageView.addGestureRecognizer(tapProfileImage)
        profilePictureImageView.layer.cornerRadius = profilePictureImageView.frame.size.height / 2
        profilePictureImageView.clipsToBounds = true
        
        let profileInfo = Helpers.getStringValueForKey(Constants.PROFILE_INFO)
        userJSON = JSON.init(parseJSON: profileInfo)
        print(userJSON)

    }
    
    func populateProfileInfo() {
        
        if let mobileNumber = userJSON["mobile_no"].string {
            self.phoneN.text = mobileNumber
            phoneNumber = mobileNumber
        }
        
        if let firstName = userJSON["first_name"].string {
          if let lastName = userJSON["last_name"].string {
            self.name.text = firstName + " " + lastName
           }
        }
        if let cData = userJSON["company_data"].dictionary{
            if let pStation = cData["company_name"]?.string{
                self.policeStation.text = pStation
            }
        }
      
        
        if let eml = userJSON["email"].string {
            self.email.text = eml
        }
        if let imageURL = userJSON["profile_pic_url"].string {
            
            // We are using Kingfisher ImageLoading Library. No need to do it manually.
            let urlString = imageURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let profilePicURL = URL(string: urlString!)
            profilePictureImageView.kf.indicatorType = .activity
            profilePictureImageView.kf.setImage(with: profilePicURL, placeholder: placeholder)
        }
        if let bpR = userJSON["bp_rank"].string{
            self.Rank.text = bpR
        }
        

    }
    
    //MARK: API Implemenation
    
    func doChangePassword(currentPass: String, newPass: String, oldPass: String){
        editProfieApiRequest = 1
        let member_id = Helpers.getIntValueForKey(Constants.MEMBER_ID)
        let params = [
            "member_id": String(member_id),
            "current_password": currentPass,
            "new_password": newPass,
            "confirm_new_password": oldPass,
            ]
        userService?.changePassword(params)
    }
    
    func getPasswordChangeResponse(_ data: [String: Any], statusCode: Int) {
        var success = false
        if statusCode == Constants.STATUS_CODE_SUCCESS{
            let response = JSON(data)
            if !response.isEmpty{
                if let passwrodChangeStatus = response["success"].bool {
                    success = passwrodChangeStatus
                    if success == true {
                        let popup = CustomAlertPopup.customOkayPopup("", msgBody: Constants.PASSWORD_CHANGE_SUCCESS_MESSAGE)
                        self.present(popup, animated: true, completion: nil)
                    }
                    else{
                        if let reson = response["message"].string {
                            let popup = CustomAlertPopup.customOkayPopup("", msgBody: reson)
                            self.present(popup, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    func doEditProfile(_ first_name: String, last_name: String, user_email: String, dateOfBirth: String, gender: String,bloodGroup: String) {
        editProfieApiRequest = 2
        let params = [
            "first_name": first_name,
            "last_name": last_name,
            "email": user_email,
            "gender": gender,
            "date_of_birth": dateOfBirth
            
        ]
        print(params)
        userService?.editProfile(params)
    }
    
    func getEditProfileResponse(_ data: [String: Any], statusCode: Int) {
        var success = false
        if statusCode == Constants.STATUS_CODE_SUCCESS{
            let response = JSON(data)
            if !response.isEmpty {
                if let updateChangeStatus = response["success"].bool {
                    success = updateChangeStatus
                    if success {
                        if let reson = response["message"].string {
                            let popup = CustomAlertPopup.customOkayPopup("", msgBody: reson)
                            self.present(popup, animated: true, completion: nil)
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            //                            Helpers.removeValue(Constants.PROFILE_INFO)
                            self.getProfile()
                        }
                        
                    }
                    else{
                        if let reson = response["message"].string {
                            let popup = CustomAlertPopup.customOkayPopup("", msgBody: reson)
                            self.present(popup, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    func doUploadProfilePicture(_ image: UIImage) {
        editProfieApiRequest = 3
        let data = image.pngData()
        let memberId = Helpers.getIntValueForKey(Constants.MEMBER_ID)
        let params = [
            "member_id": String(memberId)
        ]
        userService?.uploadProfileImage(params, imageData: data!)
    }
    
    func getUploadProfilePictureResponse(_ data: [String: Any], statusCode: Int) {
        var success = false
        if statusCode == Constants.STATUS_CODE_SUCCESS{
            let response = JSON(data)
            if !response.isEmpty{
                if let updateChangeStatus = response["success"].bool {
                    success = updateChangeStatus
                    if success {
                        if let url = response["profile_image_url"].string {
                            
                            // We are using Kingfisher ImageLoading Library. No need to do it manually.
                            let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                            let profilePicURL = URL(string: urlString!)
                            profilePictureImageView.kf.indicatorType = .activity
                            profilePictureImageView.kf.setImage(with: profilePicURL, placeholder: placeholder)
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                self.getProfile()
                            }
                        }
                    }
                    else{
                        if let reson = response["message"].string {
                            let popup = CustomAlertPopup.customOkayPopup("", msgBody: reson)
                            self.present(popup, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    func getProfile() {
        let memberId = Helpers.getIntValueForKey(Constants.MEMBER_ID)
        userService?.getProfile(memberId, from: "")
    }
    
    func getProfileResponse(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
      
            if !response.isEmpty {
                if let profileInfo = response.rawString() {
                    Helpers.setStringValueWithKey(profileInfo, key: Constants.PROFILE_INFO)
                }
            }
        }
        else {
            //need to handle this
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension EditProfileController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            let image = Commons.resizeImage(pickedImage, targetSize: CGSize(width: Constants.RESZIE_IMAGE_WIDTH, height: Constants.RESZIE_IMAGE_HEIGHT))
            currUploadedPhoto = image
            doUploadProfilePicture(currUploadedPhoto)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


extension EditProfileController: APICommunicatorDelegate {
    
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
            getEditProfileResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.SECOND {
            getUploadProfilePictureResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.THIRD {
            getPasswordChangeResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.SIXTH {
            getProfileResponse(data, statusCode: statusCode)
        }
    }
}







// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
