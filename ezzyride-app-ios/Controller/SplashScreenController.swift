//
//  SplashScreenController.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 10/27/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import LSDialogViewController
import NotificationCenter
var dipLinkUrl = ""
class SplashScreenController: UIViewController {
    @IBOutlet weak var appIdentityView: UIImageView!

    let dialogViewController = ErrorView(nibName: "serverErrorMsg", bundle: nil)
    var userJSON: JSON = [:]
    var timer = Timer()
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation? = nil
    
    var settingsService: SettingsService?
    var apiCommunicatorHelper: APICommunicator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initApiCommunicatorHelper()
        dialogViewController.splashDelegate = self
        networkReachabilityCheck()
     
      
    }

    func networkReachabilityCheck(){

        if Reachability.isConnectedToNetwork() == true {
            NotificationCenter.default.addObserver(self, selector: #selector(self.tryAgain), name: NSNotification.Name(rawValue: "serverError"), object: nil)
            setupLocationManager()
            animateView()
        }else{
            SwiftOverlays.removeAllBlockingOverlays()
            self.view?.isUserInteractionEnabled = true
           showDialog(.slideBottomTop)
        }
    }

   
    
    
    @objc func tryAgain(notif: NSNotification) {
       showDialog(.slideBottomTop)
    }
    func showDialog(_ animationPattern: LSAnimationPattern) {

        presentDialogViewController(dialogViewController, animationPattern: animationPattern)
        globalViewControllerString = "SplashScreenController"
    }
    func dismissDialog() {
        dismissDialogViewController(LSAnimationPattern.fadeInOut)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.networkReachabilityCheck()
        }
        

    }
 

  
    
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func loginSession() {
        
        let userId = Helpers.getIntValueForKey(Constants.USER_ID)
        if userId != -1 {
            
            if dipLinkUrl != "" {
                
            }else{
              navaigationToHome()
            }
        }
        else{
            navigationToGetStart()
        }
    }
    

    func navaigationToHome() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let homeController = mainStoryboard.instantiateViewController(withIdentifier: "HomeV") as? HomeController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [homeController!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    
    func navigationToGetStart() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let getStartedController = mainStoryboard.instantiateViewController(withIdentifier: "GetStartedVC") as? GetStartedController
        self.navigationController?.pushViewController(getStartedController!, animated: true)
    }
    
    @objc func timerHandler() {
        navigateController()
    }
    
    func animateView() {
        
        UIView.animate(withDuration: 1, animations: {() -> Void in
            self.appIdentityView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: {(_ finished: Bool) -> Void in
            self.checkLocationPermission()
        })
    }
    
    func navigateController() {
            loginSession()
    }
    func setupLocationManager() {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
    }
    
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        settingsService = SettingsService(self.view, communicator: apiCommunicatorHelper!)
    }
    
    func checkLocationPermission() {
        
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.delay), userInfo: nil, repeats: true)
        }
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .restricted, .denied:
                showPopup()
                break
            case .authorizedAlways, .authorizedWhenInUse:
                break
            case .notDetermined:
                break
            }
        }
        else {
            let popup = CustomAlertPopup.customPopupShow("", msgBody: Constants.LOCATION_MANAGER_INACTIVE_MESSAGE, buttonTitle: "OK")
            self.present(popup, animated: true, completion: nil)
        }
    }
    
    func showPopup() {
        let alert: UIAlertView = UIAlertView()
        alert.title = ""
        alert.message = Constants.LOCATION_SERVICE_ACCESS_DENIED
        alert.delegate = self
        alert.addButton(withTitle: "OK")
        alert.show()
    }
    
    @objc func delay() {
        if currentLocation != nil {
            timer.invalidate()
            getLatLon(currentLocation)
        }
    }
    
    func getLatLon(_ location: CLLocation? = nil) {
        
        if location != nil {
            var currLat = ""
            var currLon = ""
            if let lat = location?.coordinate.latitude {
                currLat = String(lat)
            }
            if let lon = location?.coordinate.longitude {
                currLon = String(lon)
            }
            if currLat.count > 0 && currLon.count > 0 {
                getUserZone(currLat, lon: currLon)
            }
        }
    }
    
    //MARK: API Implementation
    
    func loadSettings(_ zone: String) {
        let ZONE = zone
        let bindKeys = Constants.SETTINGS_API_KEY + Constants.SEPARATED_BY_COMA + Constants.IOS_PASSENGER_APP_KEY + Constants.SEPARATED_BY_COMA + Constants.IOS_PASSENGER_APP_UPDATE_TYPE_KEY + Constants.SEPARATED_BY_COMA + Constants.IOS_PASSENGER_APP_VERSION_CODE_KEY + Constants.SEPARATED_BY_COMA + Constants.IOS_PASSENGER_APP_TRIP_CANCEL_TEXT_KEY + Constants.SEPARATED_BY_COMA + Constants.IOS_EZZYR_HELP_LINE_KEY + Constants.SEPARATED_BY_COMA + Constants.IOS_TERMS_IMAGE_KEY + Constants.SEPARATED_BY_COMA + ZONE
        let params = [
            "keys": bindKeys
        ]
        settingsService?.loadSettings(params)
    }
    
    func getSettingsResponse(_ data: [String: Any], statusCode: Int) {
        
        if statusCode == Constants.SUCCESS_STATUS_CODE {
    
            let response = JSON(data)
            if !response.isEmpty {
                if let terms_img = response["terms_img"].string {
                    Helpers.setStringValueWithKey(terms_img, key: Constants.IOS_TERMS_IMAGE_KEY)
                }
                if let ambulanceTermImage = response["ambulance_terms_img"].string {
                    Helpers.setStringValueWithKey(ambulanceTermImage, key: Constants.AMBULANCE_TERM_IMAGE)
                    
                }
                
                if let android_passenger_app_version_code = response["android_passenger_app_version_code"].string {
                    Helpers.setStringValueWithKey(android_passenger_app_version_code, key: Constants.IOS_PASSENGER_APP_VERSION_CODE_KEY)
                }
                
                if let passenger_app_trip_cancel_text = response["passenger_app_trip_cancel_text"].dictionary {
                    if let tripCancelText = JSON(passenger_app_trip_cancel_text).rawString() {
                        Helpers.setStringValueWithKey(tripCancelText, key: Constants.IOS_PASSENGER_APP_TRIP_CANCEL_TEXT_KEY)
                    }
                }
                
                if let android_passenger_app_update_type = response["android_passenger_app_update_type"].string {
                    Helpers.setStringValueWithKey(android_passenger_app_update_type, key: Constants.IOS_PASSENGER_APP_UPDATE_TYPE_KEY)
                }
                
                if let zone_data = response["zone_data"].dictionary {
                    if let zoneData = JSON(zone_data).rawString() {
                        Helpers.setStringValueWithKey(zoneData, key: Constants.IOS_ZONE_DATA_KEY)
                    }
                }
                
                if let ezzyr_help_line = response["ezzyr_help_line"].string {
                    Helpers.setStringValueWithKey(ezzyr_help_line, key: Constants.IOS_EZZYR_HELP_LINE_KEY)
                }
                
                if let android_passenger_app = response["android_passenger_app"].string {
                    Helpers.setStringValueWithKey(android_passenger_app, key: Constants.IOS_PASSENGER_APP_KEY)
                }
                
                if let service_data = response["service_data"].dictionary {
                    if let serviceData = JSON(service_data).rawString() {
                        Helpers.setStringValueWithKey(serviceData, key: Constants.IOS_SERVICE_DATA_KEY)
                    }
                }
                navigateController()
            }
            else {
                let popup = CustomAlertPopup.customPopupShow("", msgBody: Constants.SOMETHING_WENT_WRONG, buttonTitle: "OK")
                self.present(popup, animated: true, completion: nil)
            }
        }
        else {
            // need to handle
            let popup = CustomAlertPopup.customPopupShow("", msgBody: Constants.SOMETHING_WENT_WRONG, buttonTitle: "OK")
            self.present(popup, animated: true, completion: nil)
    
        }
    }
    
    func getUserZone(_ lat: String, lon: String) {
        settingsService?.getUserZone(lat, lon: lon)
       
    }
    
    func getUserZoneResponse(_ data: [String: Any], statusCode: Int) {
        
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
            if !response.isEmpty {
                if let status = response["status"].string {
                    if status == Constants.MAP_API_STATUS_OK {
                        if let compound_code = response["plus_code"]["compound_code"].string {
                            let parts = compound_code.components(separatedBy: [" ", ","])
                            var bindZone = ""
                            let size = parts.count
                            for (index, _) in parts.enumerated().reversed()  {
                                if index > 0 {
                                    if parts[index].count > 0 {
                                        if index == size - 1 {
                                            bindZone = "Bangladesh|Dhaka"
                                        }
                                        else {
                                            bindZone = "Bangladesh|Dhaka"
                                        }
                                    }
                                }
                            }
                            loadSettings(bindZone)
                        }
                    }
                    else {
                        //need to handle
                    }
                }
                else {
                    //need to handle
                }
            }
            else {
                //need to handle
            }
        }
        else {
            //need to handle
        }
    }
}

extension SplashScreenController: APICommunicatorDelegate {
    
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
            getSettingsResponse(data, statusCode: statusCode)
            
        }
        else if methodTag == MethodTags.SECOND {
            getUserZoneResponse(data, statusCode: statusCode)
        }
    }
}

extension SplashScreenController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        currentLocation = location
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Errors: " + error.localizedDescription)
    }
}








