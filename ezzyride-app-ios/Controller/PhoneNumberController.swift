//
//  PhoneNumberController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 6/11/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

var numberForForgotPassord = ""
class PhoneNumberController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var NavigationTitle: UILabel!
    @IBOutlet weak var submit: UIButton!
    @IBOutlet weak var numberText: UITextField!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var one: UIButton!
    @IBOutlet weak var two: UIButton!
    @IBOutlet weak var Three: UIButton!
    @IBOutlet weak var four: UIButton!
    @IBOutlet weak var five: UIButton!
    @IBOutlet weak var six: UIButton!
    @IBOutlet weak var seven: UIButton!
    @IBOutlet weak var eight: UIButton!
    @IBOutlet weak var nine: UIButton!
    @IBOutlet weak var zero: UIButton!
    @IBOutlet weak var cross: UIButton!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var numberValidationMessage: UILabel!
    

    
    
    var viewTrack = 0
    var buttonTappdValue = ""
    var count = 50
    
    var ONE_TIME_PASSWORD = ""
    
    var authenticationService: AuthenticationService?
    var apiCommunicatorHelper: APICommunicator?
    var stageService: StageService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initApiCommunicatorHelper()
        uiChange()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        authenticationService = AuthenticationService(self.view, communicator: apiCommunicatorHelper!)
         stageService = StageService(self.view, communicator: apiCommunicatorHelper!)
    }
    //MARK: API Implementation for verified code

    func uiChange(){
        
        Commons.bottomCornerRedious(button: submit, cornerRedious: 20)
        let change = Commons()
        change.placeHolderColorChange(TextFild: numberText, PlaceHolder: "01XXXXXXXXX")
        change.placeHolderColorChange(TextFild: passwordText, PlaceHolder: "PASSWORD")
        numberText.inputView = UIView()
        numberText.inputAccessoryView = UIView()
        
       
    }

    
    func navigationToRegisterController() {
        let mobileNumber = numberText.text ?? ""
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let registerController = mainStoryboard.instantiateViewController(withIdentifier: "RegisterVC") as? RegisterController
        registerController?.mobileNumber = mobileNumber
        //        registerController?.authenticationCode = ONE_TIME_PASSWORD
        self.navigationController?.pushViewController(registerController!, animated: true)
    }
    
    @IBAction func numberButton(_ sender: Any) {
            if let buttonTitle = (sender as AnyObject).title(for: .normal) {
                
                let number = self.numberText.text ?? ""
                if number.count < 11 {
                    self.numberText.text?.append(buttonTitle)
                }
                changeBackOnTap(button: one)
                changeBackOnTap(button: two)
                changeBackOnTap(button: Three)
                changeBackOnTap(button: four)
                changeBackOnTap(button: five)
                changeBackOnTap(button: six)
                changeBackOnTap(button: seven)
                changeBackOnTap(button: eight)
                changeBackOnTap(button: nine)
                changeBackOnTap(button: cross)
                changeBackOnTap(button: zero)
            }
    }
    
    
    @IBAction func deleteNumber(_ sender: Any) {
        var totalNumber = self.numberText.text ?? ""
        if totalNumber.count > 0 {
            totalNumber = String(totalNumber.dropLast())
            self.numberText.text = totalNumber
        }
    }
    
    @IBAction func SubmitButton(_ sender: Any) {
        if (numberText.text?.count)! == 11 && passwordText.text!.count > 5{
            let num = numberText.text!
            let password = passwordText.text!
            doLogin(password, mobileNumber: num)
            }else{
            self.numberValidationMessage.text = "Please enter a valid phone number or Password"
            self.numberValidationMessage.textColor = UIColor.red
        }
        
    }

    func changeBackOnTap(button: UIButton){
        button.setBackgroundColor(color: UIColor(red:2, green:153, blue:52, alpha:1.0), forState: .highlighted)
    }
    
    @IBAction func Back(_ sender: Any) {
        
        if viewTrack == 0 {
            self.iniitializeGetStart()
        }else if viewTrack == 1{
            
            self.NavigationTitle.text = "Get Started"
         
            viewTrack = 0
            numberView.isHidden = false
            count = 50
        }
    }
    func getLoginResponse(_ data: [String: Any], statusCode: Int) {
        
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            print(response)
            var success = false
            if !response.isEmpty {
                if let loginStatus = response["success"].bool {
                    success = loginStatus
                    if success == true {
                        
                        if let userInfo = response.rawString() {
                            Helpers.setStringValueWithKey(userInfo, key: Constants.USER_INFO)
                        }
                        
                        if let token = response["token"].string {
                            Helpers.setStringValueWithKey(token, key: Constants.ACCESS_TOKEN)
                        }
                        
                        if let member_type = response["member_type"].int {
                            Helpers.setIntValueWithKey(member_type, key: Constants.MEMBER_TYPE)
                        }
                        else if let member_type = response["member_type"].string {
                            Helpers.setIntValueWithKey(Int(member_type)!, key: Constants.MEMBER_TYPE)
                        }
                        
                        if let member_id = response["member_id"].int {
                            Helpers.setIntValueWithKey(member_id, key: Constants.MEMBER_ID)
                            updateToken()
                        }
                        else if let member_id = response["member_id"].string {
                            Helpers.setIntValueWithKey(Int(member_id)!, key: Constants.MEMBER_ID)
                            updateToken()
                        }
                        
                        if let user_id = response["user_id"].int {
                            Helpers.setIntValueWithKey(user_id, key: Constants.USER_ID)
                        }
                        else if let user_id = response["user_id"].string {
                            Helpers.setIntValueWithKey(Int(user_id)!, key: Constants.USER_ID)
                        }
                        
                        if let stageData = response["stage_data"].dictionary {
                            if let driverStage = stageData["driver_stage"]?.int {
                                if driverStage != 0 {
                                    requestForStage()
                                    loginTace = 1
                                }else {
                                    
                                    navigateToDashboard()
                                }
                            }
                        }
                        
                    }else{
                        if let reson = response["msg"].string {
                            let popup = CustomAlertPopup.customOkayPopup("", msgBody: reson)
                            self.present(popup, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
        else {
            //need to handle
        }
    }
    func doLogin(_ password: String, mobileNumber: String) {
        
        let params = [
            "mobile_no": mobileNumber,
            "password": password,
            "user_type": String(UserType.Two.rawValue)
        ]
        authenticationService?.doLogin(params)
    }
    func updateToken(){
        
        let id = Helpers.getIntValueForKey(Constants.MEMBER_ID)
        let token = Helpers.getStringValueForKey(Constants.FCM_TOKEN)
        print("this is token\(token)")
        let params = [
            "device_id" : token,
            "member_id" : String(id)
        ]
        self.authenticationService!.doUpdateToken(params)
    }
    func requestForStage(){
        let userId = Helpers.getIntValueForKey(Constants.USER_ID)
        let params = [
            "passenger_id" : userId
        ]
        self.stageService?.doStageRequest(params)
        
    }
    
    func getUserStageDataResponse(_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
            if !response.isEmpty {
                if let stageData = response.rawString() {
                    Helpers.removeValue(Constants.STAGE_DATA)
                    Helpers.setStringValueWithKey(stageData, key: Constants.STAGE_DATA)
                    navigateToDashboard()
                }
            }
        }
    }
    func getTokenUpdteResponds(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
            print(response)
        }
    }
    func navigateToDashboard() {
        let main: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home = main.instantiateViewController(withIdentifier: "HomeV") as? HomeController
        let navigationController = UINavigationController(rootViewController: home!)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    private func iniitializeGetStart() {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SplashScreenVC") as? SplashScreenController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [viewController!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
}

extension PhoneNumberController: APICommunicatorDelegate {
    
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
            getLoginResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.ELEVENTH {
              getUserStageDataResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.FIFTH {
            getTokenUpdteResponds(data, statusCode: statusCode)
        }
    }
}













