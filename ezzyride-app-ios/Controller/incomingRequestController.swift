//
//  incomingRequestController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 14/6/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher
import CoreLocation
var victimName: String?
var victimLat : String?
var victimLng : String?
var victimDistance: Int?
var victimeAddress: String?
var victimeID : String?
var victimeImageUrl: String?

import Kingfisher
import GoogleMaps


var isInaCase = 0
class incomingRequestController: UIViewController {
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var victimeImage: UIImageView!
    @IBOutlet weak var victimeName: UILabel!
    @IBOutlet weak var imgViewMap: UIImageView!
    @IBOutlet weak var middleCirculView: UIView!
    @IBOutlet weak var rsqButton: UIButton!
    
    var homeDelegate: HomeController?
    
    let shapeLayer = CAShapeLayer()
    
    
    
    var apiCommunicatorHelper: APICommunicator?
    var AcceptRequest : BookingService?
    var countTimer:Timer!
    var counter = 28


    override func viewDidLoad() {
        super.viewDidLoad()
        initApiCommunicatorHelper()
        self.victimeName.text = victimName
        self.location.text = victimeAddress
        doRegisterForReceivingPushNotification()
        layerSetup()
        self.countTimer = Timer.scheduledTimer(timeInterval: 1 ,
                                               target: self,
                                               selector: #selector(self.invalidTime),
                                               userInfo: nil,
                                               repeats: true)
        snapShot(finalLatitude: victimLat!, finalLongitude: victimLng!)
        imgViewMap.layer.cornerRadius = 100
        imgViewMap.clipsToBounds = true
        victimeImage.layer.cornerRadius = 40
        victimeImage.clipsToBounds = true
        rsqButton.layer.cornerRadius = 25
        rsqButton.clipsToBounds = false
        showVictimeImage()
    }
    @objc func invalidTime(){
        if counter != 0{
            counter -= 1
        } else {
            countTimer.invalidate()
            navaigationToHome()
        }
        
    }
    func doRegisterForReceivingPushNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.doReceivePayload(_:)), name: .pushPayload, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    func layerSetup(){
        
        let center = view.center
        
        // create my track layer
        let trackLayer = CAShapeLayer()
        
        let circularPath = UIBezierPath(arcCenter: center, radius: 100, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        trackLayer.path = circularPath.cgPath
        
        trackLayer.strokeColor = UIColor.lightGray.cgColor
        trackLayer.lineWidth = 15
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = CAShapeLayerLineCap.round
        view.layer.addSublayer(trackLayer)
        
        //        let circularPath = UIBezierPath(arcCenter: center, radius: 100, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        shapeLayer.path = circularPath.cgPath
        
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.lineWidth = 20
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        
        shapeLayer.strokeEnd = 0
        
        view.layer.addSublayer(shapeLayer)
        handleTap()

    }
    func showVictimeImage(){
        let victimeImageUr = "http://addin-api.ezzyr.xyz/resources/profile_image/" + victimeImageUrl!
        let urlString = victimeImageUr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = URL(string: urlString!)
        victimeImage.kf.indicatorType = .activity
        victimeImage.kf.setImage(with: url, placeholder: UIImage(named: "placeImage"))
    }
    func handleTap() {
        print("Attempting to animate stroke")
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        
        basicAnimation.toValue = 1
        
        basicAnimation.duration = 30
        
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        basicAnimation.isRemovedOnCompletion = false
        
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
    }

    @objc func doReceivePayload(_ notification: NSNotification) {
        
        if let info = notification.userInfo as? [String: Any] {
            
            
            if let msg = info["message"] as? String {
                let message = JSON.init(parseJSON: msg)
                if let action = message["action"].string {
                    if action == "trip_request" {
                       
                       
                    }else if action == "ready_to_pickup" {
                    
                    }else if action == "cancel_trip_request" {
                        navaigationToHome()
                    }
                }
            }
        }
    }
    func initApiCommunicatorHelper(){
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        AcceptRequest = BookingService(self.view, communicator: apiCommunicatorHelper!)
    }

    func snapShot(finalLatitude: String, finalLongitude: String ){
        let staticMapUrl: String = "https://maps.googleapis.com/maps/api/staticmap?zoom=19&size=400x400&markers=color:blue%7Clabel:V%7C\(finalLatitude),\(finalLongitude)&key=\(Constants.GOOGLE_MAP_API_KEY)"
        print(staticMapUrl)
        let url = URL(string: staticMapUrl)
        imgViewMap.kf.indicatorType = .activity
        imgViewMap.kf.setImage(with: url, placeholder: UIImage(named: "placeImage"))
    }
    @IBAction func resque(_ sender: Any) {
         resqueVictime()
    }
 
    func resqueVictime(){
          let userId = Helpers.getIntValueForKey(Constants.USER_ID)
         let requestId = Helpers.getIntValueForKey(Constants.TRIP_REQUEST_ID)
        
     
        let params = [
            "channel_name": "chnl_dc_\(driverPhone)",
            "altitude": "0",
            "driver_id": userId,
            "passenger_id" : "\(victimeID ?? "")",
            "latitude" : victimLat ?? "",
            "request_id": requestId ,
            "longitude": victimLng ?? "",
            "vehicle_id": vehicleID ?? ""
        
            ] as [String : Any]
         print(params)
        AcceptRequest?.doResque(params)
        
       
    }
    
    func getResqueResponse(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            if !response.isEmpty {
                print(response)
                if let message = response["success"].bool{
                    if message == true{
                        isInaCase = 1
                        navaigationToHome()
                    }else{
                        
                    }
                }
            }
        }
    }
    
    func navaigationToHome() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let homeController = mainStoryboard.instantiateViewController(withIdentifier: "HomeV") as? HomeController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [homeController!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    

}

extension incomingRequestController: APICommunicatorDelegate {
    
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
        getResqueResponse(data, statusCode: statusCode)
        }
    }
}
