//
//  FareAndSubmissionViewController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 19/12/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher


class FareAndSubmissionViewController: UIViewController {

    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var cosmosView: CosmosView!
    @IBOutlet weak var submitButton: UIButton!

    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var passengerTotalFare: UILabel!
   
//    var tripId =
    
    var userStage: JSON = [:]
    var apiCommunicatorHelper: APICommunicator?
    var bookingService: BookingService?
    var stageService: StageService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Commons.makeCardView(view: ratingView)
        Commons.bottomCornerRedious(button: submitButton, cornerRedious: 5)
   
        initApiCommunicatorHelper()
        driverImage.layer.cornerRadius = 29
        driverImage.clipsToBounds = true
        let stageInfo = Helpers.getStringValueForKey(Constants.STAGE_DATA)
        userStage = JSON.init(parseJSON: stageInfo)
        
        
    }
    func requestForStage(){
        let userId = Helpers.getIntValueForKey(Constants.USER_ID)
        let params = [
            "passenger_id" : userId
        ]
        self.stageService?.doStageRequest(params)
        
    }

    func Alart(){        
        let alert = UIAlertController(title: "Have you found 999 sticker on the vehicle ?", message: "\n\n\n", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default,  handler: {
                        (action : UIAlertAction!) -> Void in
                        self.submitRating()
                    }))
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: {
                (action : UIAlertAction!) -> Void in
                self.submitRating()
            }))
        
        let image = UIImageView(image: UIImage(named: "emercency_call"))
        image.contentMode = .scaleAspectFit
        alert.view.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .centerX, relatedBy: .equal, toItem: alert.view, attribute: .centerX, multiplier: 1, constant: 0))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .centerY, relatedBy: .equal, toItem: alert.view, attribute: .centerY, multiplier: 1, constant: 0))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 130.0))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100.0))
        self.present(alert, animated: true, completion: nil)
    }
    
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        bookingService = BookingService(self.view, communicator: apiCommunicatorHelper!)
        self.stageService = StageService(self.view, communicator: apiCommunicatorHelper!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        showData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    func showData(){
        
        let stageInfo = Helpers.getStringValueForKey(Constants.STAGE_DATA)
        userStage = JSON.init(parseJSON: stageInfo)
        print(userStage)
        
        if let Data = userStage["data"].dictionary{
            if let stageData = Data["stage_data"]?.dictionary{
                if let driverStage = stageData ["driver_stage"]?.int{
                    if driverStage == 1 || driverStage == 2 || driverStage == 3 || driverStage == 4 || driverStage == 0 {
                        requestForStage()
                    }
                    
                }
                if let pessengerObj = stageData["passenger_object"]?.dictionary{
                    
                    if let stage1 = pessengerObj["stage_1"]?.dictionary {
                        if let dataRecive = stage1["data_received"]?.dictionary{
                            if let destinationName = dataRecive["dest_address"]?.string{
                               
                            }
                            if let piclocation = dataRecive["location_name"]?.string{
                               
                            }                
                        }
                    }
                    if let stage5 = pessengerObj["stage_5"]?.dictionary{
                        if let pushMsg = stage5["data_push_msg"]?.dictionary{
                            if let totalPayable = pushMsg["payable_total"]?.int{
                                self.passengerTotalFare.text = "\(totalPayable) ৳"
                            }
                            if let dicountData = pushMsg["discount"]?.string{
                                
                            }
                            if let durationData = pushMsg["duration"]?.string{
                                
                            }
                            if let baseFareData = pushMsg["base_fare"]?.string{
                                
                            }
                            if let tripFareData = pushMsg["total_fare"]?.int{
                                
                            }
                            if let tripFData = pushMsg["total_fare"]?.string {
                                
                            }
                            if let distanceData = pushMsg["distance"]?.string{
                               
                            }
                            
                            
                        }
                    }
                    
                    if let stage2_2 = pessengerObj["stage_2_2"]?.dictionary{
                        if let dataDriverDetails = stage2_2["data_driver_details"]?.dictionary{
                            if let code = dataDriverDetails["code"]?.int{
                                if code == Constants.SUCCESS_STATUS_CODE {
                         
                                    if let Profileurl = dataDriverDetails["profile_pic_url_alt"]?.string{
                                        let urlString = Profileurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                        let url = URL(string: urlString!)
                                        self.driverImage.kf.setImage(with: url)
                                    }
                                    if let activeVahicale = dataDriverDetails["active_vehicle"]?.dictionary{
                                        if let model = activeVahicale["model"]?.string{
                                            
                                        }
                                  
                                        if let vehicalType = activeVahicale["category_type"]?.string{
                                           
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                }

            }
            
        }
        
    }

    
    @IBAction func submit(_ sender: Any) {
     //   submitRating()
          Alart()
    }

    func submitRating(){
        let pID = Helpers.getIntValueForKey(Constants.USER_ID)
        let tripId = Helpers.getStringValueForKey(Constants.TRIP_ID)

        print("this is trip id\(tripId)")
        let driverId = Helpers.getIntValueForKey(Constants.DRIVER_ID)
       
        let params = [
            "passenger_id" : String(pID),
            "driver_id" : String(driverId),
            "trip_request_id": String(tripId),
            "review_value" : String(cosmosView.rating),
            "review_by":"1",
            "comments":"No comments"
        ]
        bookingService?.doReview(_params: params)
    }
    
    func getRatingReponds (_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
               print(response)
            if let success = response["success"].bool{
                if success == true{
        
                    destroyData()
                    let popup = CustomAlertPopup.customOkayPopup("", msgBody: response["message"].string!)
                    self.present(popup, animated: true, completion: nil)
                    navaigationToHome()
                    
                }else{
                    let popup = CustomAlertPopup.customOkayPopup("", msgBody: response["message"].string!)
                    self.present(popup, animated: true, completion: nil)
                }
            }
      
            
        }
        
    }
    // User Stage Responds
    func getUserStageDataResponse(_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
            
            if !response.isEmpty {
                if let stageData = response.rawString() {
                    Helpers.removeValue(Constants.STAGE_DATA)
                    Helpers.setStringValueWithKey(stageData, key: Constants.STAGE_DATA)
                    showData()
                }
            }
        }
    }
    
    func destroyData(){
        Helpers.removeValue(Constants.TRIP_ID)
        Helpers.removeValue(Constants.DRIVER_ID)
        Helpers.removeValue(Constants.STAGE_DATA)
    }
    func navaigationToHome() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let homeController = mainStoryboard.instantiateViewController(withIdentifier: "HomeV") as? HomeController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [homeController!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    

}

extension  FareAndSubmissionViewController: APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST{
          getRatingReponds(data, statusCode: statusCode)
        } else if methodTag == MethodTags.ELEVENTH {
            getUserStageDataResponse(data, statusCode: statusCode)
        }

        
    }
    
    
}

