//
//  GreatOfferController.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 11/4/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit

class GreatOfferController: UIViewController {

    @IBAction func skipButtonPressed(_ sender: UIButton) {
        
        let introSlider: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let getStartedController = introSlider.instantiateViewController(withIdentifier: "GetStartedVC") as? GetStartedController
        self.navigationController?.pushViewController(getStartedController!, animated: true)
    }
    
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        
        let introSlider: UIStoryboard = UIStoryboard(name: "IntroSlider", bundle: nil)
        let carOnDemandController = introSlider.instantiateViewController(withIdentifier: "CarOnDemandVC") as? CarOnDemandController
        self.navigationController?.pushViewController(carOnDemandController!, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}
