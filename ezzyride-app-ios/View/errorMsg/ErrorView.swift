//
//  ErrorView.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 3/3/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit

var globalViewControllerString = ""
class ErrorView: UIViewController {

    var splashDelegate: SplashScreenController?
    var homeDelegate: HomeController?

    var editProfileDelegate: EditProfileController?
    
    @IBOutlet weak var tryButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tryButton.layer.cornerRadius = 20
        self.tryButton.clipsToBounds = true

        // Do any additional setup after loading the view.
    }
    
    @IBAction func tryAgain(_ sender: Any) {
        if globalViewControllerString == "SplashScreenController"{
            splashDelegate?.dismissDialog()
        }else if globalViewControllerString == "HomeController" {
            homeDelegate?.dismissInternetDialog()
        }else if globalViewControllerString == "UpComminingTripController" {
    
        }else if globalViewControllerString == "UpcommingDetailsController"{
     
        }else if globalViewControllerString == "TripHistoryController"{
          
        }else if globalViewControllerString == "PromotionController"{
  
        }else if globalViewControllerString == "EditProfileController" {
            editProfileDelegate?.dismissInternetDialog()
        }else if globalViewControllerString == "notificationController"{
          
        }
    }
    

}
