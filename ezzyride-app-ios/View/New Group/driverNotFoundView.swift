//
//  driverNotFoundView.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 5/3/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit

class driverNotFoundView: UIViewController {
    var homeViewDelegate: HomeController?
    override func viewDidLoad() {
        super.viewDidLoad()

        view.bounds.size.height = UIScreen.main.bounds.size.height * 0.4
        view.bounds.size.width = UIScreen.main.bounds.size.width * 0.8
        // Do any additional setup after loading the view.
    }
    
    @IBAction func close(_ sender: Any) {
        homeViewDelegate?.dismissDialog()
    }
}
