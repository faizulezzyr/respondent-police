//
//  BookingSuccessView.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 27/2/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import Lottie

class SuccessView: UIViewController {
    @IBOutlet weak var text: UILabel!
    
    @IBOutlet weak var successBooking: UIView!
    var delegate: HomeController?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.successBooking.layer.cornerRadius = 50
        self.successBooking.clipsToBounds = true
        view.bounds.size.height = UIScreen.main.bounds.size.height * 0.5
        view.bounds.size.width = UIScreen.main.bounds.size.width * 0.8
        successAnimation()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func close(_ sender: Any) {
        delegate?.dismissDialog()
        
    }

    
    func successAnimation(){
        let animationView = AnimationView(name: "success")
        animationView.frame = CGRect(x: 0, y: 0, width: self.successBooking.frame.width, height: self.successBooking.frame.height)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        self.successBooking.addSubview(animationView)
        animationView.play()
    }

}
