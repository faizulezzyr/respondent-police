//
//  DriverConfirmView.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 27/2/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import Lottie

class DriverConfirmView: UIViewController {
    @IBOutlet weak var successView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.successView.layer.cornerRadius = 50
        self.successView.clipsToBounds = true
        view.bounds.size.height = UIScreen.main.bounds.size.height * 0.5
        view.bounds.size.width = UIScreen.main.bounds.size.width * 0.8
        successAnimation()
    }
    
    @IBAction func close(_ sender: Any) {
     
    }
    func successAnimation(){
        let animationView = AnimationView(name: "success")
        animationView.frame = CGRect(x: 0, y: 0, width: self.successView.frame.width, height: self.successView.frame.height)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        self.successView.addSubview(animationView)
        animationView.play()
    }


}
