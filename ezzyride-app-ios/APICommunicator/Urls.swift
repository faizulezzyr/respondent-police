//
//  Urls.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 25/4/17.
//  Copyright © 2017 WebAlive. All rights reserved.
//

import Foundation

class Urls: NSObject {
    
    static var BASE_URL = "http://addin-api.ezzyr.xyz/v1"
   // static var BASE_URL = "http://13.127.60.179/v1"
    
    //static var BOOKING_BASE_URL = "http://13.127.60.179/mp1"
    static var BOOKING_BASE_URL = "http://services.ezzyr.com/mp1"
    static var SETTING_MAIN_URL = "/settings-main"
    static var PHONE_NUMBER_VARIFICATION_URL = "/check-mobile-no"
    static var LOGIN_URL = "/login"
    static var VALIDATE_CODE = "/check-verification-code"
    static var USER_REGISTER_URL = "/save-profile-data-passenger"
    static var USER_PROFILE_URL = "/profile/member/"
    static var EDIT_PROFILE_URL = "/profile_update/member/"
    static var UPLOAD_PROFILE_IMAGE_URL = "/profile/image"
    static var CHANGE_PASSWORD = "/change-password"
    static var FORGOT_PASSWORD_OTP_REQUEST = "/forgot-password-request"
    static var RESET_PASSWORD = "/forgot-password"
    static var TRIP_REQUEST_URL = "/trip-request/"
    static var CALCULATE_FARE_URL = "/calculate-default-rent-dev"
    static var CANCEL_TRIP_REQUEST = "/cancel-trip-request-driver"
    static var TRIP_HISTORY = "/date-wise-report/respondent/"
    static var PER_PAGE = "&per_page=10"
    static var TRIP_DETAILS = "/trip-details/"
    static var STAGE_DATA = "/user-current-statge"
    static var TRIP_RATING = "/trip-review"
    static var PROMO_CODE = "/passenger/apply-promo-code"
    static var GET_PROMO = "/promo-code/passenger/"
    static var MESSAGE = "/get-contents-list"
    static var UPDETE_TOKEN = "/update-device-id"
    static var ACCEPT_BY_DRIVER = "/request-acceptor-diver-details/"
    static var FAKE_CAR  = "/gps_coordinate/"
    static var BOOK_LONG_ROAD = "/booking-request"
    static var BOOKING_LIST = "/passenger-booking-list"
    static var BOOKING_DRIVER_LIST = "/applied-driver-list"
    static var DRIVER_CONFIRM = "/passenger-confirm-driver"
    static var CANCEL_BOOKING = "/cancel-trip-request-passenger"
    static var AMBULANCE_LIST = "/emergency-vehicle-incity-dev"
    static var ONLINE = "/driver-on-off"
    static var UPDATE_GPS_CORDINATE = "/gps_coordinate"
    static var RESQUE = "/trip-request-response"
    static var NotifYRescue = "/ready-to-pick-notify"
    // GOOGLE MAP API
    static var GOOGLE_MAP_GEOGOCE_BASE_URL = "https://maps.googleapis.com/maps/api/geocode/json?"
    static var GOOGLE_MAP_DIRECTION_API_BASE_URL = "https://maps.googleapis.com/maps/api/directions/json?"
    static var GOOGLE_PLACES_SEARCH_API_BASE_URL = "https://maps.googleapis.com/maps/api/place/textsearch/json?"
    static var GOOGLE_MAP_DISTANCE_MATRIX_API_BASE_URL = "https://maps.googleapis.com/maps/api/distancematrix/json?"
    
    
    // URL ENCODED KEYS
    static var ORIGIN_KEY = "origin="
    static var DESTINATION_KEY = "&destination="
    static var API_KEY = "&mode=driving&key="
    static var QUERY_KEY = "query="
    static var REGION_KEY = "&region="
    static var REGION_VALUE = "BD"
    static var PLACES_API_KEY = "&key="
    static var UNITS_KEY = "units="
    static var UNIT_VALUE_IN_KILOMETERS = "metric"
    static var UNIT_VALUE_IN_MILES = "imperial"
    static var AND = "&"
    
    
    /// menu url
    
    static var menuUrl = "http://partner.ezzyr.com"
}





