//
//  SettingsService.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 11/7/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class SettingsService: NSObject {
    
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    
    func loadSettings(_ params: Parameters) {
        let url = Urls.BASE_URL + Urls.SETTING_MAIN_URL
        print(url)
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.FIRST, withHeader: false)
        SwiftOverlays.removeAllOverlaysFromView(self.view!)
    }
    
    func getUserZone(_ lat: String, lon: String) {
       
        let latlng = lat + "," + lon
        let url = Urls.GOOGLE_MAP_GEOGOCE_BASE_URL + "latlng=" + latlng + "&key=" + Constants.GOOGLE_MAP_API_KEY
        print(url)
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.SECOND, withHeader: false)
        SwiftOverlays.removeAllOverlaysFromView(self.view!)
      
    }
}








