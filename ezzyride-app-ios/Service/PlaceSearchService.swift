//
//  PlaceSearchService.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 12/1/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class PlaceSearchService: NSObject {
    
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    
    func doAutoCompleteSearch(_ query: String) {
        
        let url = Urls.GOOGLE_PLACES_SEARCH_API_BASE_URL + Urls.QUERY_KEY + query + Urls.PLACES_API_KEY + Constants.GOOGLE_MAP_API_KEY + Urls.REGION_KEY + Urls.REGION_VALUE
        apiCommunicatorHelper?.getDataFromGoogleAPI(url, methodTag: MethodTags.FOURTH, withHeader: false)
    }
    
    func getUserZone(_ lat: String, lon: String) {
        let latlng = lat + "," + lon
        let url = Urls.GOOGLE_MAP_GEOGOCE_BASE_URL + "latlng=" + latlng + "&key=" + Constants.GOOGLE_MAP_API_KEY
        apiCommunicatorHelper?.getDataFromGoogleAPI(url, methodTag: MethodTags.SECOND, withHeader: false)
    }
    
    func getUserSource(_ lat: String, lon: String) {
        let latlng = lat + "," + lon
        let url = Urls.GOOGLE_MAP_GEOGOCE_BASE_URL + "latlng=" + latlng + "&key=" + Constants.GOOGLE_MAP_API_KEY
        print(url)
        apiCommunicatorHelper?.getDataFromGoogleAPI(url, methodTag: MethodTags.EIGHTH, withHeader: false)
    }
    
    func getUserDestination(_ lat: String, lon: String) {
        let latlng = lat + "," + lon
        let url = Urls.GOOGLE_MAP_GEOGOCE_BASE_URL + "latlng=" + latlng + "&key=" + Constants.GOOGLE_MAP_API_KEY
        apiCommunicatorHelper?.getDataFromGoogleAPI(url, methodTag: MethodTags.NINETH, withHeader: false)
    }
}
