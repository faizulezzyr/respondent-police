//
//  BookingService.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 12/7/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class BookingService: NSObject {
    
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    
    func doBookingRequest(_ params: Parameters) {
        let url = Urls.BASE_URL + Urls.TRIP_REQUEST_URL
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.SEVENTH, withHeader: true)
    }

    func doResque(_ params: Parameters){
      let url = Urls.BASE_URL + Urls.RESQUE
     apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.FIRST, withHeader: true)
        
    }
    
    func doReview(_params: Parameters){
        let url = Urls.BASE_URL + Urls.TRIP_RATING
        apiCommunicatorHelper?.getDataByPOST(url, params: _params, methodTag: MethodTags.FIRST, withHeader: true)
    }
    func doNotifyRescue(_params: Parameters){
        let url = Urls.BASE_URL + Urls.NotifYRescue
        print(url)
        apiCommunicatorHelper?.getDataByPOST(url, params: _params, methodTag: MethodTags.SEVENTH, withHeader: true)
    }
    func doCancelRequest(_ params: Parameters) {
        let url = Urls.BASE_URL + Urls.CANCEL_TRIP_REQUEST
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.EIGHTH, withHeader: true)
    }

}
