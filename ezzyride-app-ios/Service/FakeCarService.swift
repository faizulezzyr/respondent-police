//
//  FakeCarService.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 26/1/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import Alamofire
class FakeCarService: NSObject {
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    

    func getFakeCarList(lat: String, lng: String){
       let url =   Urls.BASE_URL + Urls.FAKE_CAR + "latitude/" + lat + "/longitude/" + lng
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.THIRTEEN, withHeader: true)
    }

}
